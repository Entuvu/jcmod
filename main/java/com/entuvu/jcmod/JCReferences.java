package com.entuvu.jcmod;

public class JCReferences {
	public static final String MODID = "jcmod";
    public static final String VERSION = "a0.3.3";
    public static final String NAME = "JC Mod";
    
    public static final boolean TESTMODE = true;
}
