package com.entuvu.jcmod.core.event;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import com.entuvu.jcmod.core.JCSoundType;
import com.entuvu.jcmod.core.register.RegisterItem;

import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.BlockDirt;
import net.minecraft.block.BlockGrass;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockLog;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSeeds;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class JCEventHandler {
	
	public void entityDropItem(ItemStack itemStackIn, BlockPos pos, World world)
    {
            EntityItem entityitem = new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), itemStackIn);
            world.spawnEntityInWorld(entityitem);
    }
	
	@SubscribeEvent
	public void onHarvestDrops(HarvestDropsEvent event)
	{
		Random rand = new Random();
	    World world = event.world;
	    Block block = event.state.getBlock();
	    IBlockState blockState = event.state;
	    BlockPos blockPos = event.pos;
	    
	    if (!world.isRemote && world.getGameRules().getGameRuleBooleanValue("doTileDrops") && !event.isSilkTouching) {
			// Set meta to -100 unless you need to specify a meta value for block state.
			if(blockState == Blocks.stone.getDefaultState()){											       // Smooth Stone - Meta = 0
				DropSpecial.harvestEventDrop(false, true, 0, 2, RegisterItem.rock, -100, event, blockPos);
				if(rand.nextInt(20) == 1){
					DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.copper_nugget, -100, event, blockPos);
				}
			}
			else if(blockState == Blocks.stone.getStateFromMeta(1)){  									       // Granite - Meta = 1
				DropSpecial.harvestEventDrop(false, true, 0, 2, RegisterItem.rock_granite, -100, event, blockPos);
				if(rand.nextInt(15) == 1){
					DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.gold_nugget, -100, event, blockPos);
				}
			}
			else if(blockState == Blocks.stone.getStateFromMeta(3)){								           // Diorite - Meta = 3
				DropSpecial.harvestEventDrop(false, true, 0, 2, RegisterItem.rock_diorite, -100, event, blockPos);
				if(rand.nextInt(15) == 1){
					DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.tin_nugget, -100, event, blockPos);
				}
			}
			else if(blockState == Blocks.stone.getStateFromMeta(5)){									        // Andesite - Meta = 5
				DropSpecial.harvestEventDrop(false, true, 0, 2, RegisterItem.rock_andesite, -100, event, blockPos);
				if(rand.nextInt(15) == 1){
					DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.iron_nugget, -100, event, blockPos);
				}
			}
			else if(block == Blocks.iron_ore){																   // Iron Ore	
				DropSpecial.harvestEventDrop(false, true, 1, 1, RegisterItem.iron_chunk, -100, event, blockPos);
				DropSpecial.harvestEventDrop(false, false, 0, 1, RegisterItem.rock, -100, event, blockPos);
			}
			else if(block == Blocks.gold_ore){																   // Gold Ore
				DropSpecial.harvestEventDrop(false, true, 1, 1, RegisterItem.gold_chunk, -100, event, blockPos);
				DropSpecial.harvestEventDrop(false, false, 0, 1, RegisterItem.rock, -100, event, blockPos);
			}
			else if(block == Blocks.coal_ore){																   // Coal Ore
				DropSpecial.harvestEventDrop(false, false, 1, 1, RegisterItem.rock, -100, event, blockPos);				
			}
			
			
			if(block instanceof BlockDirt){	// Dirt Block
				DropSpecial.harvestEventDrop(false, true, 0, 2, RegisterItem.pile_dirt, -100, event, blockPos);
				rand = new Random();
	        	int randomDrop = rand.nextInt(10);
	        	if(randomDrop == 1){
	        		DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.rock, -100, event, blockPos);
	        	}
	        	randomDrop = rand.nextInt(15);
		        if(randomDrop == 1){
		        	DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.grub, -100, event, blockPos);
	        	}
			}
			
			if(block instanceof BlockGrass){ // Grass Dirt Block
				DropSpecial.harvestEventDrop(false, true, 0, 1, RegisterItem.pile_dirt, -100, event, blockPos);
				rand = new Random();
	        	int randomDrop = rand.nextInt(30);
	        	if(randomDrop == 1){
	        		DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.rock, -100, event, blockPos);
	        	}
	        	randomDrop = rand.nextInt(25);
		        if(randomDrop == 1){
		        	DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.grub, -100, event, blockPos);
	        	}
			}
			
			if (block instanceof BlockLeaves){ //Tree Leaves
	        	rand = new Random();
	        	int randomDrop = rand.nextInt(10);
	        	if(randomDrop == 1){
	        		DropSpecial.harvestEventDrop(false, false, 1, 1, RegisterItem.branch, -100, event, blockPos);
	        	}
	        }
	       
	        if (block instanceof BlockBush) { //Dead Bush
	        	/* Don't allow wheat seeds to be dropped from grass, we still get wheat seed drop from wheat itself */
	        	if(block == Blocks.tallgrass){
		        	Iterator itr = event.drops.iterator();
		        	while(itr.hasNext()){
		        		ItemStack item = (ItemStack) itr.next();
		        		if(item.getItem() == Items.wheat_seeds){
		        			itr.remove();
		        		}
		        	}
	        	}
	          	
	        	/* We check if the BlockBush is a Dead Bush, and drop sticks instead of thatch */
	        	if (blockState == Blocks.deadbush.getDefaultState()){
	        		DropSpecial.harvestEventDrop(false, false, 0, 1, Items.stick, -100, event, blockPos);
	        	} else {
	        		rand = new Random();
	        		int randomDrop = rand.nextInt(10);
	        		if(randomDrop == 1) {
	        			DropSpecial.harvestEventDrop(false, false, 1, 1, RegisterItem.thatch, -100, event, blockPos);
	        		}	
	        	}
	        }
	        
	        if (block == Blocks.gravel){ // Gravel Block	
	        	DropSpecial.harvestEventDrop(false, true, 0, 2, RegisterItem.pile_gravel, -100, event, blockPos);
	        	rand = new Random();
	        	int randomDrop = rand.nextInt(8);
	        	if(randomDrop == 1){
	        		DropSpecial.harvestEventDrop(false, false, 1, 0, Items.flint, -100, event, blockPos);
	        	}
	        	randomDrop = rand.nextInt(12);
		        if(randomDrop == 1){
		        	DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.bone_shards, -100, event, blockPos);
	        	}
	        }
	        
	        if(blockState == Blocks.sand.getDefaultState()){ // Sand Block - Meta = 0
				DropSpecial.harvestEventDrop(false, true, 0, 2, RegisterItem.pile_sand, -100, event, blockPos);
	        	rand = new Random();
	        	int randomDrop = rand.nextInt(25);
	        	if(randomDrop == 1) {
	        		DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.quartz_shards, -100, event, blockPos);
	        	}
	        } else if(blockState == Blocks.sand.getStateFromMeta(1)){ // Red Sand Block - Meta = 1
				DropSpecial.harvestEventDrop(false, true, 0, 2, RegisterItem.pile_red_sand, -100, event, blockPos);
	        	Random rand2 = new Random();
	        	int randomDrop2 = rand2.nextInt(25);
	        	if(randomDrop2 == 1){
	        		DropSpecial.harvestEventDrop(false, false, 1, 0, RegisterItem.quartz_shards, -100, event, blockPos);
	        	}
	        }	
	        	        	        
	        if (block == Blocks.sandstone){ // Sandstone Block	
	        	DropSpecial.harvestEventDrop(false, true, 1, 2, RegisterItem.brick_sand, -100, event, blockPos);
	        	DropSpecial.harvestEventDrop(false, false, 0, 1, RegisterItem.pile_sand, -100, event, blockPos);
	        }	        
	        
	        if (block == Blocks.red_sandstone){ // Red Sandstone Block	
	        	DropSpecial.harvestEventDrop(false, true, 1, 2, RegisterItem.brick_red_sand, -100, event, blockPos);
	        	DropSpecial.harvestEventDrop(false, false, 0, 1, RegisterItem.pile_red_sand, -100, event, blockPos);
	        }
	        
	        if (block == Blocks.brick_block){ // Brick Block	
	        	DropSpecial.harvestEventDrop(false, true, 1, 2, Items.brick, -100, event, blockPos);
	        	DropSpecial.harvestEventDrop(false, false, 0, 1, Items.clay_ball, -100, event, blockPos);
	        }
	        
	        if (block == Blocks.hardened_clay){ // Hardened Clay Block	
	        	DropSpecial.harvestEventDrop(false, true, 1, 2, RegisterItem.clay_shards, -100, event, blockPos);	        	
	        }	
	        	        
	    }
	    
	}

	public static final Map<EntityPlayer, BlockPos> brokenAgainAndAgain = new HashMap<>();
	public int blockCounter = 0;

	@SubscribeEvent
	public void onBlockBreak(BlockEvent.BreakEvent event){
		BlockPos blockPos = event.pos;
		Block block = event.state.getBlock();
		EntityPlayer player = event.getPlayer();
		ItemStack heldItemStack = player.getHeldItem();
		
		// If the player is in creative mode, then let it break.
		if(player.capabilities.isCreativeMode){
			return;
		}
		
		int neededHarvestLevel = block.getHarvestLevel(event.state);
		String neededToolClass = block.getHarvestTool(event.state);
		
		int usedHarvestLevel = 0;
		String usedToolClass = null;
		
		if(heldItemStack != null){
			for (String toolClass : heldItemStack.getItem().getToolClasses(heldItemStack)){
				int h1 = heldItemStack.getItem().getHarvestLevel(heldItemStack, toolClass);
				if(h1 >= usedHarvestLevel){
					usedHarvestLevel = h1;
					usedToolClass = toolClass;
				}
			}
		}

		if(neededToolClass == null || neededToolClass.equalsIgnoreCase("shovel") || neededToolClass.equalsIgnoreCase("null")){
			return;
		}
		
		if(neededToolClass.equalsIgnoreCase(usedToolClass)){
			if(usedHarvestLevel < neededHarvestLevel){
				event.setCanceled(true);
				return;
			}
		}else{
			if(usedToolClass == null || usedToolClass.equalsIgnoreCase("null")){
				if(!brokenAgainAndAgain.containsKey(player) || brokenAgainAndAgain.get(player)==null || !brokenAgainAndAgain.get(player).equals(event.pos)){
					brokenAgainAndAgain.put(player, event.pos);
					blockCounter = 1;
					event.setCanceled(true);
					return;
				}else{
					if(blockCounter < 3){
						blockCounter ++;
						event.setCanceled(true);
						return;
					}else{
						blockCounter = 0;
						if(heldItemStack == null && block instanceof BlockLog){
							event.setCanceled(true);
							return;
						}else{
							brokenAgainAndAgain.put(player, null);	
						}
					}
				}
			}else{
				event.setCanceled(true);
				return;
			}
		}
	}

	
}
