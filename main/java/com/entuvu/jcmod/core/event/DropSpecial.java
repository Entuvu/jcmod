package com.entuvu.jcmod.core.event;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;

public class DropSpecial {
	/**
     * Drop an item into the world
     * 
     * @param alwaysDrop Set an int value of guaranteed amount of items dropped.
     * @param additionallyDrop This is an int that could randomly drop above the set drop. So always + random(additional)
     * @param itemToDrop This is the item we wish to drop.
     * @param event This is the event we are using : should be harvest drop event
     * @param pos The pos we want to drop the special item.
     */
	public static void drop(int alwaysDrop, int additionallyDrop, Item itemToDrop, int itemMeta, BlockPos pos, World world) {
		int numberToDrop = alwaysDrop;
		
		if(additionallyDrop != 0){
			Random rand = new Random();
			numberToDrop = rand.nextInt(((alwaysDrop + additionallyDrop) - alwaysDrop) + 1) + alwaysDrop;
		}
		
		if(itemMeta != -100)
			world.spawnEntityInWorld(new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(itemToDrop, numberToDrop, itemMeta )));
		else
			world.spawnEntityInWorld(new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(itemToDrop, numberToDrop)));
	}
	
	
	
	/**
     * Drop an item into the world
     *  
     * @param dropNothing This is set to true if we are simply not dropping anything.
     * @param preventVanillaDrop This is set to true if we are preventing the vanilla drop.
     * @param alwaysDrop Set an int value of guaranteed amount of items dropped.
     * @param additionallyDrop This is an int that could randomly drop above the set drop. So always + random(additional)
     * @param itemToDrop This is the item we wish to drop.
     * @param event This is the event we are using : should be harvest drop event
     * @param pos The pos we want to drop the special item.
	 * @return 
     */
	public static void harvestEventDrop(boolean dropNothing, boolean preventVanillaDrop, int alwaysDrop, int additionallyDrop, Item itemToDrop, int itemMeta, HarvestDropsEvent event, BlockPos pos) {

		if(preventVanillaDrop || dropNothing){
			event.drops.clear();
		}
		
		if(dropNothing)
			return;

		drop(alwaysDrop, additionallyDrop, itemToDrop, itemMeta, pos, event.world);
	}

	public static void dropblock(int alwaysDrop, int additionallyDrop, Block blockToDrop, int itemMeta, BlockPos pos, World world) {
		int numberToDrop = alwaysDrop;
		
		if(additionallyDrop != 0){
			Random rand = new Random();
			numberToDrop = rand.nextInt(((alwaysDrop + additionallyDrop) - alwaysDrop) + 1) + alwaysDrop;
		}
		
		if(itemMeta != -100)
			world.spawnEntityInWorld(new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(blockToDrop, numberToDrop, itemMeta )));
		else
			world.spawnEntityInWorld(new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(blockToDrop, numberToDrop)));
		
	}
	
}
