package com.entuvu.jcmod.core.guicontainer;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerTileEntityAlloyFurnace extends Container {

	/*
	 * SLOTS:
	 * Alloy Furnace 0-4 ...... 0  - 4  [ 0 = burn-able-1, 1 = alloy-1, 2 = burn-able-2, 3 = alloy-2, 4 = output ]
	 * Player Inventory 9-35 .. 5  - 31
	 * Player Inventory 0-8 ... 32 - 40
	 */
	
	private IInventory alloyFurnaceInventory;
	
	public ContainerTileEntityAlloyFurnace(InventoryPlayer playerInv, IInventory alloyFurnaceInventory) {
		this.alloyFurnaceInventory = alloyFurnaceInventory;
		
		this.addSlotToContainer(new Slot(this.alloyFurnaceInventory, 0, 52, 53));	/* Left Burn-able */
		this.addSlotToContainer(new Slot(this.alloyFurnaceInventory, 1, 52, 18));	/* Left Alloy */
		this.addSlotToContainer(new Slot(this.alloyFurnaceInventory, 2, 106, 53));	/* Right Burn-able */
		this.addSlotToContainer(new Slot(this.alloyFurnaceInventory, 3, 106, 18));	/* Right Alloy */
		this.addSlotToContainer(new Slot(this.alloyFurnaceInventory, 4, 79, 59));	/* Output */
		
		/* Player Inventory 9-35 .. 5  - 31 */
	    for (int y = 0; y < 3; ++y) {
	        for (int x = 0; x < 9; ++x) {
	            this.addSlotToContainer(new Slot(playerInv, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
	        }
	    }

	    /* Player Inventory 0-8 ... 32 - 40 */
	    for (int x = 0; x < 9; ++x) {
	        this.addSlotToContainer(new Slot(playerInv, x, 8 + x * 18, 142));
	    }
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return this.alloyFurnaceInventory.isUseableByPlayer(playerIn);
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int fromSlot) {
		ItemStack previous = null;
		Slot slot = (Slot) this.inventorySlots.get(fromSlot);
		
		if(slot != null && slot.getHasStack()){
			ItemStack current = slot.getStack();
			previous = current.copy();
			
			// Custom Start
			if(fromSlot < 5){
				// From TE inventory to player inventory
				if(!this.mergeItemStack(current, 5, 41, true))
					return null;
			}else{
				// From Player inventory to TE Inventory
				if(!this.mergeItemStack(current, 0, 5, false))
					return null;
			}
			// Custom End
			if(current.stackSize == 0)
				slot.putStack((ItemStack)null);
			else
				slot.onSlotChanged();
			
			if(current.stackSize == previous.stackSize)
				return null;
			slot.onPickupFromSlot(playerIn, current);
		}
		
		return previous;
	}

}
