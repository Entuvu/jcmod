package com.entuvu.jcmod.core.guicontainer;

import com.entuvu.jcmod.core.tileentity.TileEntityThatchBasket;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerTileEntityThatchBasket extends Container {
	/*
	 * SLOTS:
	 * 
	 * Tile Entity 0-5 ........ 0  - 5
	 * Player Inventory 9-35 .. 6  - 34
	 * Player Inventory 0-8 ... 35 - 43
	 */
	
	private TileEntityThatchBasket te;
	
	public ContainerTileEntityThatchBasket(IInventory playerInv, TileEntityThatchBasket te){
		this.te = te;
		
		// Tile Entity, Slot 0-5, Slot IDs 0-5
		/* y = rows, x = columns */
	    for (int y = 0; y < 2; ++y) {
	        for (int x = 0; x < 3; ++x) {
	            this.addSlotToContainer(new Slot(te, x + y * 3, 70 + x * 18, 17 + y * 18));
	        }
	    }

	    // Player Inventory, Slot 9-35, Slot IDs 6-32
	    for (int y = 0; y < 3; ++y) {
	        for (int x = 0; x < 9; ++x) {
	            this.addSlotToContainer(new Slot(playerInv, x + y * 9 + 9, 8 + x * 18, 66 + y * 18));
	        }
	    }

	    // Player Inventory, Slot 0-8, Slot IDs 33-41
	    for (int x = 0; x < 9; ++x) {
	        this.addSlotToContainer(new Slot(playerInv, x, 8 + x * 18, 124));
	    }
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return this.te.isUseableByPlayer(playerIn);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int fromSlot) {
		ItemStack previous = null;
		Slot slot = (Slot) this.inventorySlots.get(fromSlot);
		
		if(slot != null && slot.getHasStack()){
			ItemStack current = slot.getStack();
			previous = current.copy();
			
			// Custom Start
			if(fromSlot < 6){
				// From TE inventory to player inventory
				if(!this.mergeItemStack(current, 6, 42, true))
					return null;
			}else{
				// From Player Inventory to TE Inventory
				if(!this.mergeItemStack(current, 0, 6, false))
					return null;
			}
			// Custom End
			if(current.stackSize == 0)
				slot.putStack((ItemStack)null);
			else
				slot.onSlotChanged();
			
			if(current.stackSize == previous.stackSize)
				return null;
			slot.onPickupFromSlot(playerIn, current);
		}
		
		return previous;
	}
}
