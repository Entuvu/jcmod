package com.entuvu.jcmod.core.guicontainer;

import com.entuvu.jcmod.core.inventory.MixingBoxSlotContainer;
import com.entuvu.jcmod.core.testing.TileEntityTestMixer;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerTileEntityTestMixer extends Container {
	
	private IInventory mixingBoxInventory;

	public ContainerTileEntityTestMixer(InventoryPlayer inventory, TileEntityTestMixer tileEntity) {
		this.mixingBoxInventory = (IInventory)tileEntity;
		
	this.addSlotToContainer(new MixingBoxSlotContainer(this.mixingBoxInventory, 0, 29, 34));
		for(int x = 0; x < 2; ++ x){
			for(int y = 0; y < 2; ++ y){
				this.addSlotToContainer(new Slot(this.mixingBoxInventory, 1 + x + y * 2, 55 + x * 18,  25 + y * 18));
			}
		}
		
		this.addSlotToContainer(new Slot(this.mixingBoxInventory, 5, 128, 35));
		
		/* Player Inventory 9-35 .. 5  - 31 */
	    for (int y = 0; y < 3; ++y) {
	        for (int x = 0; x < 9; ++x) {
	            this.addSlotToContainer(new Slot(inventory, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
	        }
	    }

	    /* Player Inventory 0-8 ... 32 - 40 */
	    for (int x = 0; x < 9; ++x) {
	        this.addSlotToContainer(new Slot(inventory, x, 8 + x * 18, 142));
	    }
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return this.mixingBoxInventory.isUseableByPlayer(playerIn);
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int fromSlot) {
		ItemStack previous = null;
		Slot slot = (Slot) this.inventorySlots.get(fromSlot);
		
		if(slot != null && slot.getHasStack()){
			ItemStack current = slot.getStack();
			previous = current.copy();
			
			// Custom Start
			if(fromSlot < 6){
				// From TE inventory to player inventory
				if(!this.mergeItemStack(current, 6, 42, true))
					return null;
			}else{
				// From Player inventory to TE Inventory
				if(!this.mergeItemStack(current, 0, 6, false))
					return null;
			}
			// Custom End
			if(current.stackSize == 0)
				slot.putStack((ItemStack)null);
			else
				slot.onSlotChanged();
			
			if(current.stackSize == previous.stackSize)
				return null;
			slot.onPickupFromSlot(playerIn, current);
		}
		
		return previous;
	}
}
