package com.entuvu.jcmod.core.guicontainer;

import com.entuvu.jcmod.core.crafting.MixerRecipes;
import com.entuvu.jcmod.core.register.RegisterBlock;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

public class ContainerTileEntityMixer extends Container {

	/*
	 * SLOTS:
	 * 
	 * Tile Entity 0-5 ........ 0  - 5
	 * Player Inventory 9-35 .. 6  - 32
	 * Player Inventory 0-8 ... 33 - 41
	 */	
	
	public InventoryCrafting craftMatrix = new InventoryCrafting(this, 2,2 );
	public IInventory craftResult = new InventoryCraftResult();
	private World worldObj;
	private BlockPos blockPos;

	public ContainerTileEntityMixer(InventoryPlayer playerInventory, World worldIn, BlockPos blockPos) {
		this.worldObj = worldIn;
		this.blockPos = blockPos;
		
		this.addSlotToContainer(new SlotCrafting(playerInventory.player, this.craftMatrix, this.craftResult, 0, 128, 35));
		
		int i;
        int j;
        
        for (i = 0; i < 2; ++i)
        {
            for (j = 0; j < 2; ++j)
            {
                this.addSlotToContainer(new Slot(this.craftMatrix, j + i * 2, 55 + j * 18, 25 + i * 18));
            }
        }

        for (i = 0; i < 3; ++i)
        {
            for (j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInventory, i, 8 + i * 18, 142));
        }

        this.onCraftMatrixChanged(this.craftMatrix);
        System.out.println("OUT 4");
	}
	
	@Override
	public void onCraftMatrixChanged(IInventory inventoryIn) {
		this.craftResult.setInventorySlotContents(0, MixerRecipes.getInstance().findMatchingRecipe(this.craftMatrix, this.worldObj));
	}
	
	@Override
	public void onContainerClosed(EntityPlayer playerIn) {
		super.onContainerClosed(playerIn);

        if (!this.worldObj.isRemote)
        {
            for (int i = 0; i < 4; ++i)
            {
                ItemStack itemstack = this.craftMatrix.getStackInSlotOnClosing(i);

                if (itemstack != null)
                {
                    playerIn.dropPlayerItemWithRandomChoice(itemstack, false);
                }
            }
        }
        System.out.println("OUT 6");
	}
	

	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
    {
        return this.worldObj.getBlockState(this.blockPos).getBlock() != RegisterBlock.mixer ? false : playerIn.getDistanceSq((double)this.blockPos.getX() + 0.5D, (double)this.blockPos.getY() + 0.5D, (double)this.blockPos.getZ() + 0.5D) <= 64.0D;
    }
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (index == 0)
            {
                if (!this.mergeItemStack(itemstack1, 5, 41, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }
            else if (index >= 5 && index < 32)
            {
                if (!this.mergeItemStack(itemstack1, 32, 41, false))
                {
                    return null;
                }
            }
            else if (index >= 32 && index < 41)
            {
                if (!this.mergeItemStack(itemstack1, 5, 32, false))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 5, 41, false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(playerIn, itemstack1);
        }

        return itemstack;
	}
	
	@Override
	 public boolean canMergeSlot(ItemStack p_94530_1_, Slot p_94530_2_)
    {
        return p_94530_2_.inventory != this.craftResult && super.canMergeSlot(p_94530_1_, p_94530_2_);
    }

}
