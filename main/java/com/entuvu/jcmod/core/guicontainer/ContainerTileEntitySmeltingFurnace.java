package com.entuvu.jcmod.core.guicontainer;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerTileEntitySmeltingFurnace extends Container {
	
	/*
	 * SLOTS:
	 * Smelting Furnace 0-4 ... 0  - 4  [ 0 = chunk1, 1 = chunk2, 2 = mold, 3 = burnable, 4 = output ]
	 * Player Inventory 9-35 .. 5  - 31
	 * Player Inventory 0-8 ... 32 - 40
	 */

	private IInventory smeltingFurnaceInventory;
	
	public ContainerTileEntitySmeltingFurnace(InventoryPlayer inventory, IInventory tileEntity) {
		this.smeltingFurnaceInventory = tileEntity;
		
		this.addSlotToContainer(new Slot(this.smeltingFurnaceInventory, 0, 52, 18 )); //Chunk 1
		this.addSlotToContainer(new Slot(this.smeltingFurnaceInventory, 1, 70, 18 )); //Chunk 2
		this.addSlotToContainer(new Slot(this.smeltingFurnaceInventory, 2, 106, 18 )); //Ingot Mold  96  58
		this.addSlotToContainer(new Slot(this.smeltingFurnaceInventory, 3, 62, 59 )); //Burnable 
		this.addSlotToContainer(new Slot(this.smeltingFurnaceInventory, 4, 107, 60 )); //Output
		
		/* Player Inventory 9-35 .. 5-31 */
		for(int y = 0; y < 3; ++y){
			for (int x = 0; x < 9; ++x) {
	            this.addSlotToContainer(new Slot(inventory, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
	        }
		}
		
		/* Player Inventory 0-8 ... 32 - 40 */
	    for (int x = 0; x < 9; ++x) {
	        this.addSlotToContainer(new Slot(inventory, x, 8 + x * 18, 142));
	    }
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return this.smeltingFurnaceInventory.isUseableByPlayer(playerIn);
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int fromSlot) {
		ItemStack previous = null;
		Slot slot = (Slot) this.inventorySlots.get(fromSlot);
		
		if(slot != null && slot.getHasStack()){
			ItemStack current = slot.getStack();
			previous = current.copy();
			
			// Custom Start
			if(fromSlot < 5){
				// From TE inventory to player inventory
				if(!this.mergeItemStack(current, 5, 41, true))
					return null;
			}else{
				// From Player inventory to TE Inventory
				if(!this.mergeItemStack(current, 0, 5, false))
					return null;
			}
			// Custom End
			if(current.stackSize == 0)
				slot.putStack((ItemStack)null);
			else
				slot.onSlotChanged();
			
			if(current.stackSize == previous.stackSize)
				return null;
			slot.onPickupFromSlot(playerIn, current);
		}
		
		return previous;
	}

}
