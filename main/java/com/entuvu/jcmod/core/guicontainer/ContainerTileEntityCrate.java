package com.entuvu.jcmod.core.guicontainer;

import com.entuvu.jcmod.core.tileentity.TileEntityCrate;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerTileEntityCrate extends Container {
	/*
	 * SLOTS:
	 * 
	 * Tile Entity 0-8 ........ 0  - 8
	 * Player Inventory 9-35 .. 9  - 35
	 * Player Inventory 0-8 ... 36 - 44
	 */
	
	private TileEntityCrate te;
	
	public ContainerTileEntityCrate(IInventory playerInv, TileEntityCrate te){
		this.te = te;
		
		// Tile Entity, Slot 0-8, Slot IDs 0-8
		for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                this.addSlotToContainer(new Slot(te, j + i * 3, 62 + j * 18, 17 + i * 18));
            }
        }

	    // Player Inventory, Slot 9-35, Slot IDs 9-35
	    for (int i = 0; i < 3; ++i) {
	        for (int j = 0; j < 9; ++j) {
	        	this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 75 + i * 18));
	        }
	    }

	    // Player Inventory, Slot 0-8, Slot IDs 36-44
	    for (int x = 0; x < 9; ++x) {
	    	this.addSlotToContainer(new Slot(playerInv, x, 8 + x * 18, 133));
	    }
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return this.te.isUseableByPlayer(playerIn);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int fromSlot) {
		ItemStack previous = null;
		Slot slot = (Slot) this.inventorySlots.get(fromSlot);
		
		if(slot != null && slot.getHasStack()){
			ItemStack current = slot.getStack();
			previous = current.copy();
			
			// Custom Start
			if(fromSlot < 9){
				// From TE inventory to player inventory
				if(!this.mergeItemStack(current, 9, 45, true))
					return null;
			}else{
				// From Player Inventory to TE Inventory
				if(!this.mergeItemStack(current, 0, 9, false))
					return null;
			}
			// Custom End
			if(current.stackSize == 0)
				slot.putStack((ItemStack)null);
			else
				slot.onSlotChanged();
			
			if(current.stackSize == previous.stackSize)
				return null;
			slot.onPickupFromSlot(playerIn, current);
		}
		
		return previous;
	}
}
