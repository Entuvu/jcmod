package com.entuvu.jcmod.core.tabs;

import com.entuvu.jcmod.core.register.RegisterItem;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class JCCreativeTab extends CreativeTabs {

	public JCCreativeTab(int index, String label) {
		super(index, label);
	}

	@Override
	public Item getTabIconItem() {
		return RegisterItem.flint_axe;
	}
	
}
