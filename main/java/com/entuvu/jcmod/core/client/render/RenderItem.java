package com.entuvu.jcmod.core.client.render;

import com.entuvu.jcmod.JCReferences;
import com.entuvu.jcmod.core.register.RegisterItem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;

public final class RenderItem {
	
	public static void registerItemRenderer(){
		// Ingots //
		render(RegisterItem.bronze_ingot);
		render(RegisterItem.copper_ingot);
		render(RegisterItem.lead_ingot);
		render(RegisterItem.silver_ingot);
		render(RegisterItem.steel_ingot);
		render(RegisterItem.tin_ingot);
		
		// Molds //
		render(RegisterItem.clay_ingot_mold);
		render(RegisterItem.clay_bar_mold);
		render(RegisterItem.clay_nail_mold);
		render(RegisterItem.clay_arrowhead_mold);
		render(RegisterItem.clay_hinge_mold);
		render(RegisterItem.ingot_mold);
		render(RegisterItem.bar_mold);
		render(RegisterItem.nail_mold);
		render(RegisterItem.arrowhead_mold);
		render(RegisterItem.hinge_mold);
		
		// Other Metal items //
		render(RegisterItem.bronze_sheet);
		render(RegisterItem.copper_sheet);
		render(RegisterItem.gold_sheet);
		render(RegisterItem.tin_sheet);
		render(RegisterItem.copper_nugget);
		render(RegisterItem.gold_nugget);
		render(RegisterItem.iron_nugget);
		render(RegisterItem.tin_nugget);
		render(RegisterItem.tin_can);
		render(RegisterItem.copper_bar);
		render(RegisterItem.gold_bar);
		render(RegisterItem.iron_bar);
		render(RegisterItem.tin_bar);
		render(RegisterItem.iron_nails);
		render(RegisterItem.copper_arrowhead);
		render(RegisterItem.iron_hinge);
		
		// Material Chunks //
		render(RegisterItem.tin_chunk);
		render(RegisterItem.copper_chunk);
		render(RegisterItem.gold_chunk);
		render(RegisterItem.iron_chunk);
		render(RegisterItem.lead_chunk);
		render(RegisterItem.silver_chunk);
		
		// Material ROCKS //
		render(RegisterItem.rock);
		render(RegisterItem.rock_andesite);
		render(RegisterItem.rock_diorite);
		render(RegisterItem.rock_granite);		
		render(RegisterItem.brick_sand);
		render(RegisterItem.brick_red_sand);
		render(RegisterItem.brick_stone);
		
		// Material Cut //
		//reg(JCItems.wood_plank);
		render(RegisterItem.wood_sheet);
		render(RegisterItem.plank_oak);
		render(RegisterItem.plank_birch);
		render(RegisterItem.plank_acacia);
		render(RegisterItem.plank_jungle);
		render(RegisterItem.plank_spruce);
		render(RegisterItem.plank_darkoak);		
		
		// Materials Natural //
		render(RegisterItem.branch);
		render(RegisterItem.thatch);
		render(RegisterItem.twine);
		render(RegisterItem.jute);
		render(RegisterItem.mortar);
		render(RegisterItem.compost);
		render(RegisterItem.fertilizer);
		render(RegisterItem.bucket_mortar);
		render(RegisterItem.bucket_compost);
		render(RegisterItem.sack_fertilizer);
		render(RegisterItem.burlap_sack);
		render(RegisterItem.clay_shards);
		render(RegisterItem.flint_shards);
		render(RegisterItem.quartz_shards);
		render(RegisterItem.bone_shards);
		render(RegisterItem.pile_gravel);
		render(RegisterItem.pile_sand);
		render(RegisterItem.pile_red_sand);
		render(RegisterItem.pile_dirt);
		
		// Material FIRE //		
		render(RegisterItem.kindling);
		
		// Material Tools //
		render(RegisterItem.firestarter);
		render(RegisterItem.wood_sieve);
		render(RegisterItem.wood_handle);		
		render(RegisterItem.stone_pickaxe_head);		
		render(RegisterItem.stone_hoe_head);
		render(RegisterItem.stone_shovel_head);
		render(RegisterItem.stone_axe_head);
		/*reg(JCItems.copper_sword_hilt);
		reg(JCItems.copper_sword_blade);
		reg(JCItems.copper_pick_head);
		reg(JCItems.copper_hoe_head);
		reg(JCItems.copper_shovel_head);
		reg(JCItems.copper_axe_head);*/
		render(RegisterItem.copper_saw_blade);
		render(RegisterItem.bronze_sword_hilt);
		render(RegisterItem.bronze_sword_blade);
		render(RegisterItem.bronze_pick_head);
		render(RegisterItem.bronze_hoe_head);
		render(RegisterItem.bronze_shovel_head);
		render(RegisterItem.bronze_axe_head);
		//reg(JCItems.bronze_saw_blade);
		render(RegisterItem.iron_hammer_head);
		render(RegisterItem.iron_sword_hilt);
		render(RegisterItem.iron_sword_blade);
		render(RegisterItem.iron_pick_head);
		render(RegisterItem.iron_hoe_head);
		render(RegisterItem.iron_shovel_head);
		render(RegisterItem.iron_axe_head);
		render(RegisterItem.iron_saw_blade);
				
		// Tools Picks //
		//reg(JCItems.copper_pick);
		render(RegisterItem.bronze_pick);
		
		// Tools Axes //
		render(RegisterItem.flint_axe);
		//reg(JCItems.copper_axe);
		render(RegisterItem.bronze_axe);
		
		// Tools Hoes //
		//reg(JCItems.copper_hoe);
		render(RegisterItem.bronze_hoe);
		
		// Tools Shovel //
		//reg(JCItems.copper_shovel);
		render(RegisterItem.bronze_shovel);
		
		// Tools Weapons //
		render(RegisterItem.flint_knife);
		render(RegisterItem.copper_knife);
		render(RegisterItem.iron_knife);
		render(RegisterItem.wood_spear);
		render(RegisterItem.copper_spear);
		//reg(JCItems.copper_sword);
		render(RegisterItem.bronze_sword);
		
		// Custom Tools //
		render(RegisterItem.copper_saw);
		render(RegisterItem.copper_chisel);
		//reg(JCItems.bronze_saw);
		//reg(JCItems.bronze_chisel);
		render(RegisterItem.iron_hammer);
		render(RegisterItem.iron_saw);
		render(RegisterItem.iron_chisel);
		
		// Armor //
		render(RegisterItem.grass_chestplate);
		render(RegisterItem.grass_helmet);	   
	    render(RegisterItem.grass_leggings);
	    render(RegisterItem.grass_boots);
		
		// Food //
	    render(RegisterItem.chocolate_bar);
	    render(RegisterItem.grub);
	    render(RegisterItem.boiled_egg);
	    
	    // Misc. Items //
	    render(RegisterItem.jc_book);
	    render(RegisterItem.can_grubs);
	    
	    /* plants */
	    render(RegisterItem.jute_seed);
	}
	
	private static void render(Item metaItem, int i, String string) {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
		.register(metaItem, i, new ModelResourceLocation(JCReferences.MODID + ":" + string, "inventory"));
	}

	public static void render(Item item){
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
		.register(item, 0, new ModelResourceLocation(JCReferences.MODID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	
}
