package com.entuvu.jcmod.core.client.render;

import com.entuvu.jcmod.JCReferences;
import com.entuvu.jcmod.core.register.RegisterBlock;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;

public class RenderBlock {	
	public static void renderBlocks(){
		/* ores */
		render(RegisterBlock.tin_ore);
		render(RegisterBlock.copper_ore);
		render(RegisterBlock.lead_ore);
		render(RegisterBlock.silver_ore);
		
		/* stone */ 
		render(RegisterBlock.cobblestone_andesite);
		render(RegisterBlock.cobblestone_diorite);
		render(RegisterBlock.cobblestone_granite);
		
		/* stairs */
		render(RegisterBlock.stairs_thatch);
		render(RegisterBlock.stairs_andesite);
		render(RegisterBlock.stairs_diorite);
		render(RegisterBlock.stairs_granite);
		
	    /* other decorative */
	    render(RegisterBlock.thatch_block);
	    render(RegisterBlock.sod);
	    
	    /* dynamic */
	    render(RegisterBlock.wood_palisade_bottom);
		render(RegisterBlock.wood_palisade_top);
		render(RegisterBlock.wood_palisade);
		render(RegisterBlock.wall_log_left);	  
	    render(RegisterBlock.wall_log_middle);
	    render(RegisterBlock.wall_log_right);
	    /* wood */
		render(RegisterBlock.wood_frame);
	    render(RegisterBlock.wood_slat_bottom);
	    render(RegisterBlock.wood_slat_middle);
	    render(RegisterBlock.wood_slat_top);
	    render(RegisterBlock.wood_slat_birch_bottom);
	    render(RegisterBlock.wood_slat_birch_middle);
	    render(RegisterBlock.wood_slat_birch_top);
	    render(RegisterBlock.wood_slat_acacia_bottom);
	    render(RegisterBlock.wood_slat_acacia_middle);
	    render(RegisterBlock.wood_slat_acacia_top);
	    render(RegisterBlock.wood_slat_jungle_bottom);
	    render(RegisterBlock.wood_slat_jungle_middle);
	    render(RegisterBlock.wood_slat_jungle_top);
		
	    /* wood stone */
		render(RegisterBlock.wood_stone);
	    render(RegisterBlock.wood_stone_fancy);
	    render(RegisterBlock.wood_stone_r);
	    render(RegisterBlock.wood_stone_l);
	    render(RegisterBlock.wood_stone_h);
	    render(RegisterBlock.wood_stone_v);
	    render(RegisterBlock.wood_stone_no_top);
	    render(RegisterBlock.wood_stone_no_bottom);
	    render(RegisterBlock.wood_stone_no_left);
	    render(RegisterBlock.wood_stone_no_right);

	    /* tileentity blocks */
	    render(RegisterBlock.alloy_furnace);
	    render(RegisterBlock.smelting_furnace);
	    render(RegisterBlock.mixer);
	    render(RegisterBlock.brick_oven);
	    render(RegisterBlock.wood_stump);
	    render(RegisterBlock.butchers_table);
	    render(RegisterBlock.artisan_bench);
	    render(RegisterBlock.crate);
	    render(RegisterBlock.thatch_basket);
	    render(RegisterBlock.camp_fire);
	    render(RegisterBlock.stonecutters_bench);
	    
	    /* special purpose blocks */
	    render(RegisterBlock.topsoil);
	    
	    /* plants */
	    render(RegisterBlock.jute_plant);
	}
	
	public static void render(Block block){
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
		.register(Item.getItemFromBlock(block),0,new ModelResourceLocation(JCReferences.MODID + ":" + block.getUnlocalizedName().substring(5),"inventory"));
	}
	
	public static void render(Block block, int meta, String file) {
	    Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
	    .register(Item.getItemFromBlock(block), meta, new ModelResourceLocation(JCReferences.MODID + ":" + file, "inventory"));
	}
	
	
}
