package com.entuvu.jcmod.core.client.gui;

import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityThatchBasket;
import com.entuvu.jcmod.core.tileentity.TileEntityThatchBasket;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;

public class GuiTileEntityThatchBasket extends GuiContainer {

	private IInventory playerInv;
	private TileEntityThatchBasket te;
	
	public GuiTileEntityThatchBasket(IInventory playerInv, TileEntityThatchBasket te) {
		super(new ContainerTileEntityThatchBasket(playerInv, te));
		this.playerInv = playerInv;
		this.te = te;
		
		this.xSize = 176;
		this.ySize = 166;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.color(1.0f, 1.0f, 1.0f,1.0f);
		this.mc.getTextureManager().bindTexture(new ResourceLocation("jcmod:textures/gui/container/thatch_basket_gui.png"));
		this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize,this.ySize);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		String s = this.te.getDisplayName().getUnformattedText();
		this.fontRendererObj.drawString(s, 88 - this.fontRendererObj.getStringWidth(s)/2, 6, 4210752);
		this.fontRendererObj.drawString(this.playerInv.getDisplayName().getUnformattedText(), 8, 54, 4210752);
	}
}
