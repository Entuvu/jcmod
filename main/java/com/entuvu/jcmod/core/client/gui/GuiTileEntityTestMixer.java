package com.entuvu.jcmod.core.client.gui;

import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityTestMixer;
import com.entuvu.jcmod.core.testing.TileEntityTestMixer;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiTileEntityTestMixer extends GuiContainer {

	private static final ResourceLocation mixingBoxGuiTexture = new ResourceLocation("jcmod:textures/gui/container/mixing_box_gui.png");
	private IInventory mixingBoxInventory;
	private InventoryPlayer playerInventory;

	public GuiTileEntityTestMixer(InventoryPlayer inventory, TileEntityTestMixer tileEntity) {
		super(new ContainerTileEntityTestMixer(inventory, tileEntity));
		this.mixingBoxInventory = (IInventory)tileEntity;
		this.playerInventory = inventory;
		
		this.xSize = 176;
		this.ySize = 166;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		this.mc.getTextureManager().bindTexture(mixingBoxGuiTexture);
		
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		String s= this.mixingBoxInventory.getDisplayName().getUnformattedText();
		this.fontRendererObj.drawString(s,  8 , 6 ,  4210752);
		this.fontRendererObj.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, 74, 4210752);
	}

}
