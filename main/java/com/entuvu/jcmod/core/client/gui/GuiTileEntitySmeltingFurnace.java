package com.entuvu.jcmod.core.client.gui;

import com.entuvu.jcmod.core.guicontainer.ContainerTileEntitySmeltingFurnace;
import com.entuvu.jcmod.core.tileentity.TileEntitySmeltingFurnace;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiTileEntitySmeltingFurnace extends GuiContainer {
	
	public static final ResourceLocation smeltingFurnaceGuiTexture = new ResourceLocation("jcmod:textures/gui/container/smelting_furnace_gui.png");
	
	private final InventoryPlayer playerInventory;
	private IInventory smeltingFurnaceInventory;

	public GuiTileEntitySmeltingFurnace(InventoryPlayer inventory, IInventory tileEntity) {
		super(new ContainerTileEntitySmeltingFurnace(inventory, tileEntity));
		this.playerInventory = inventory;
		this.smeltingFurnaceInventory = tileEntity;
		
		this.xSize = 176;
		this.ySize = 166;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.color(1.0f, 1.0f, 1.0f,1.0f);
		this.mc.getTextureManager().bindTexture(smeltingFurnaceGuiTexture);
		
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize,this.ySize);
		
		 /* drawTexturedModalRect ( screenX, screenY, textureX, textureY, width, height) */
		
		int i1;

        if (TileEntitySmeltingFurnace.isBurning(this.smeltingFurnaceInventory))
        {
            i1 = this.getFire(13);
            this.drawTexturedModalRect(k + 63, l + 43 + 13 - i1, 176, 13 - i1, 13, i1 + 1); //Fire is drawing correctly
            this.drawTexturedModalRect(k + 89, l + 25, 204, 0, 14, 3); //Red bar is drawing correctly
        }
        
        i1 = this.func_175381_h(17);
       
        this.drawTexturedModalRect(k + 107, l + 37, 189, 0, 15, i1); //Yellow Arrow is drawing correctly but 1 yellow row remains at the top
        
	}
	
    private int func_175381_h(int p_175381_1_)
    {
        int j = this.smeltingFurnaceInventory.getField(2);
        int k = this.smeltingFurnaceInventory.getField(3);
        return k != 0 && j != 0 ? j * p_175381_1_ / k : 0;
    }

    private int getFire(int heightOfImage)
    {
        int totalTimeToBurnItem = this.smeltingFurnaceInventory.getField(1);
        
        if(totalTimeToBurnItem == 0){
        	totalTimeToBurnItem = 1;
        }

        /*
        if (totalTimeToBurnItem == 0)
        {
            totalTimeToBurnItem = 200;
        }
        */

        // Grab the current furnace burn time and multiply it by our height and divide by the total item burn time.
        return this.smeltingFurnaceInventory.getField(0) * heightOfImage / totalTimeToBurnItem;
    }

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		String s = this.smeltingFurnaceInventory.getDisplayName().getUnformattedText();
		
		this.fontRendererObj.drawString(s, 30, 6, 4210752); //Crafting Device Name
		this.fontRendererObj.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, 74, 4210752); //"Inventory" label at bottom left
	}

}
