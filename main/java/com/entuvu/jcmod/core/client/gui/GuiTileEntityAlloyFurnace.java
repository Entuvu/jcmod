package com.entuvu.jcmod.core.client.gui;

import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityAlloyFurnace;
import com.entuvu.jcmod.core.tileentity.TileEntityAlloyFurnace;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiTileEntityAlloyFurnace extends GuiContainer{
	private static final ResourceLocation alloyFurnaceGuiTexture = new ResourceLocation("jcmod:textures/gui/container/alloy_furnace_gui.png");
	private final InventoryPlayer playerInventory;
	private IInventory alloyFurnaceInventory;
	
	public GuiTileEntityAlloyFurnace(InventoryPlayer playerInventory, IInventory alloyFurnaceInventory) {
		super(new ContainerTileEntityAlloyFurnace(playerInventory, alloyFurnaceInventory));
		this.alloyFurnaceInventory = alloyFurnaceInventory;
		this.playerInventory = playerInventory;
		
		this.xSize = 176;
		this.ySize = 166;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.color(1.0f, 1.0f, 1.0f,1.0f);
		this.mc.getTextureManager().bindTexture(alloyFurnaceGuiTexture);
		
		int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
        
        int i1;
        
        boolean burn1 = false;
        boolean burn2 = false;
        
        if(TileEntityAlloyFurnace.isBurning(alloyFurnaceInventory, TileEntityAlloyFurnace.LEFT_FUEL)){
        	i1 = this.getFire(13, TileEntityAlloyFurnace.LEFT_FUEL);
        	this.drawTexturedModalRect(k + 53, l + 37 + 13 - i1, 176, 13 - i1, 13, i1 ); //Fire  
        	burn1 = true;
        }
        
        if(TileEntityAlloyFurnace.isBurning(alloyFurnaceInventory, TileEntityAlloyFurnace.RIGHT_FUEL)){
        	i1 = this.getFire(13, TileEntityAlloyFurnace.RIGHT_FUEL);
        	this.drawTexturedModalRect(k + 107, l + 37 + 13 - i1, 176, 13 - i1, 13, i1 ); //Fire  
        	burn2 = true;
        }
        
        i1 = this.func_175381_h(22);
        
        this.drawTexturedModalRect(k + 79, l + 30, 189, 0, 15, i1); //Yellow Arrow
        if(burn1 && burn2){
        	this.drawTexturedModalRect(k + 72, l + 25, 204, 0, 30, 2); //Yellow Arrow
        }
	}
	
	 private int func_175381_h(int p_175381_1_)
	    {
	        int j = this.alloyFurnaceInventory.getField(4);
	        int k = this.alloyFurnaceInventory.getField(5);
	        return k != 0 && j != 0 ? j * p_175381_1_ / k : 0;
	    }
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		String s = this.alloyFurnaceInventory.getDisplayName().getUnformattedText();
		
		this.fontRendererObj.drawString(s, 60, 6, 4210752); //Crafting Device Name
		this.fontRendererObj.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, 74, 4210752); //"Inventory" label at bottom left
	}
	
	private int getFire(int heightOfImage, int field)
    {
        int totalTimeToBurnItem = this.alloyFurnaceInventory.getField(field +1);
        // Grab the current alloy furnace burn time and multiply it by our height and divide by the total item burn time.
        return this.alloyFurnaceInventory.getField(field) * heightOfImage / totalTimeToBurnItem;       
        
	}

}
