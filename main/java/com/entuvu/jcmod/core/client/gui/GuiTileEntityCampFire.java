package com.entuvu.jcmod.core.client.gui;

import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityCampFire;
import com.entuvu.jcmod.core.tileentity.TileEntityCampFire;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiTileEntityCampFire extends GuiContainer{
	private static final ResourceLocation campFireGuiTexture = new ResourceLocation("jcmod:textures/gui/container/camp_fire_gui.png");
	private final InventoryPlayer playerInventory;
	private IInventory tileCampFire;
	
	public GuiTileEntityCampFire(InventoryPlayer playerInv, IInventory campFireInventory) {
		super(new ContainerTileEntityCampFire(playerInv, campFireInventory));
		this.playerInventory = playerInv;
		this.tileCampFire = campFireInventory;
		
		this.xSize = 176;
		this.ySize = 166;
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.color(1.0f, 1.0f, 1.0f,1.0f);
		this.mc.getTextureManager().bindTexture(campFireGuiTexture);
		
		int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
        
        /* drawTexturedModalRect ( screenX, screenY, textureX, textureY, width, height) */
		
		int i1;

		//System.out.println(TileEntityCampFire.isBurning(this.tileCampFire));
        if (TileEntityCampFire.isBurning(this.tileCampFire))
        {
            i1 = this.getFire(13);
            this.drawTexturedModalRect(k + 63, l + 42 + 13 - i1, 176, 13 - i1, 13, i1 + 1); //Fire             
        }
        
        i1 = this.func_175381_h(23);
       
        this.drawTexturedModalRect(k + 88, l + 39, 189, 0, i1, 22); //Yellow Arrow
        
	}
	
    private int func_175381_h(int p_175381_1_)
    {
        int j = this.tileCampFire.getField(2);
        int k = this.tileCampFire.getField(3);
        return k != 0 && j != 0 ? j * p_175381_1_ / k : 0;
    }

    private int getFire(int heightOfImage)
    {
        int totalTimeToBurnItem = this.tileCampFire.getField(1);

        /*
        if (totalTimeToBurnItem == 0)
        {
            totalTimeToBurnItem = 200;
        }
        */

        // Grab the current campfire burn time and multiply it by our height and divide by the total item burn time.
        return this.tileCampFire.getField(0) * heightOfImage / totalTimeToBurnItem;       
        
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		String s = this.tileCampFire.getDisplayName().getUnformattedText();
		this.fontRendererObj.drawString(s, 88 - this.fontRendererObj.getStringWidth(s)/2, 6, 4210752); //Crafting device name
		this.fontRendererObj.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, 74, 4210752); //"Inventory" label at bottom left
	}

	
}
