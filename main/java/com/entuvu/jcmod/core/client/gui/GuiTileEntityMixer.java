package com.entuvu.jcmod.core.client.gui;

import com.entuvu.jcmod.core.guicontainer.ContainerTileEntityMixer;
import com.entuvu.jcmod.core.tileentity.TileEntityMixer;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiTileEntityMixer extends GuiContainer {

	private static final ResourceLocation mixerGuiTexture = new ResourceLocation("jcmod:textures/gui/container/mixing_box_gui.png");
	private InventoryPlayer playerInventory;
	private IInventory tileMixer;

	public GuiTileEntityMixer(InventoryPlayer playerInv, World worldIn, BlockPos blockPos ) {
		super(new ContainerTileEntityMixer(playerInv, worldIn, blockPos ));
		
		this.playerInventory = playerInv;
		this.tileMixer = (TileEntityMixer)worldIn.getTileEntity(blockPos);
		
		this.xSize = 176;
		this.ySize = 166;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.color(1.0f, 1.0f, 1.0f,1.0f);
		this.mc.getTextureManager().bindTexture(mixerGuiTexture);
		
		int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);		
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		String s = this.tileMixer.getDisplayName().getUnformattedText();
		this.fontRendererObj.drawString(s, 88 - this.fontRendererObj.getStringWidth(s)/2, 6, 4210752); //Crafting device name
		this.fontRendererObj.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, 74, 4210752); //"Inventory" label at bottom left
	}

}
