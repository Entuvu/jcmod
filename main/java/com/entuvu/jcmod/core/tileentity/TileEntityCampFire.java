package com.entuvu.jcmod.core.tileentity;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.crafting.CampFireRecipes;
import com.entuvu.jcmod.core.crafting.JCRecipeBuddy;
import com.entuvu.jcmod.core.network.PacketCampFire;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.gui.IUpdatePlayerListBox;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.MathHelper;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityCampFire extends TileEntity implements IInventory, IUpdatePlayerListBox {
	private ItemStack[] campFireInventory;
	private String customName;
	
	public int furnaceBurnTime;
	public int currentItemBurnTime;
	
	public int cookTime;
	public int totalCookTime;
	
	public TileEntityCampFire(){
		this.campFireInventory = new ItemStack[this.getSizeInventory()];
	}
	
	@Override
	public String getName() {
		return this.hasCustomName() ? this.customName : "container.jcmod_camp_fire";
	}
	
	@Override
	public boolean hasCustomName() {
		return this.customName != null && !this.customName.equals("");
	}
	
	@Override
	public IChatComponent getDisplayName() {
		return this.hasCustomName() ? new ChatComponentText(this.getName()) : new ChatComponentTranslation(this.getName());
	}

	@Override
	public int getSizeInventory() {
		return 4;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		if(index < 0 || index >= this.getSizeInventory())
			return null;
		return this.campFireInventory[index];
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count) {
		if(this.getStackInSlot(index) != null){
			ItemStack itemstack;
			if(this.getStackInSlot(index).stackSize <= count){
				itemstack = this.getStackInSlot(index);
				this.setInventorySlotContents(index, null);
				this.markDirty();
				return itemstack;
			}else{
				itemstack = this.getStackInSlot(index).splitStack(count);
				if(this.getStackInSlot(index).stackSize<= 0){
					this.setInventorySlotContents(index, null);
				}else{
					// Just to show that changes happened
					this.setInventorySlotContents(index, this.getStackInSlot(index));
				}
				
				this.markDirty();
				return itemstack;
			}
		}else{
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int index) {
		ItemStack stack = this.getStackInSlot(index);
		this.setInventorySlotContents(index, null);
		return stack;
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		boolean flag = stack != null && stack.isItemEqual(this.campFireInventory[index]) && ItemStack.areItemStackTagsEqual(stack, this.campFireInventory[index]);
        this.campFireInventory[index] = stack;

        if (stack != null && stack.stackSize > this.getInventoryStackLimit())
        {
            stack.stackSize = this.getInventoryStackLimit();
        }

        if (index == 0 && !flag)
        {
        	// This is important - this sets the total cook time to the cook time of the item and resets the cook time to zero
        	// When an item is placed in the slot.
            this.totalCookTime = this.getCookTime(stack);
            this.cookTime = 0;
            this.markDirty();
        }
	}
	
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}
	
	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.getPos()) == this && player.getDistanceSq(this.pos.add(0.5,0.5,0.5)) <= 64;
	}
	
	@Override
	public void openInventory(EntityPlayer player) {
		
	}

	@Override
	public void closeInventory(EntityPlayer player) {
		
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		 return index == 2 ? false : (index != 1 ? true : isItemFuel(stack) || SlotFurnaceFuel.isBucket(stack));
	}
	
	private boolean isItemFuel(ItemStack stack) {
		return getItemBurnTime(stack) > 0;
	}
	
	@Override
	 public int getField(int id)
    {
        switch (id)
        {
            case 0:
                return this.furnaceBurnTime;
            case 1:
                return this.currentItemBurnTime;
            case 2:
                return this.cookTime;
            case 3:
                return this.totalCookTime;
            default:
                return 0;
        }
    }
	
	@Override
    public void setField(int id, int value)
    {
        switch (id)
        {
            case 0:
                this.furnaceBurnTime = value;
                break;
            case 1:
                this.currentItemBurnTime = value;
                break;
            case 2:
                this.cookTime = value;
                break;
            case 3:
                this.totalCookTime = value;
                break;
        }
    }
	
	@Override
    public int getFieldCount()
    {
        return 4;
    }
	
	@Override
	public void clear() {
		for(int i = 0; i<this.getSizeInventory(); i++)
			this.setInventorySlotContents(i, null);
	}
	
	public void setCustomName(String customName){
		this.customName = customName;
	}
	
	@Override
	public void update() {
		// sets burning to the is burning state (if fire burn time is greater than 0)
		boolean burning = this.isBurning();
		boolean needMarkDirty = false;
		
		// tick down burn
		if(this.isBurning()){
			--this.furnaceBurnTime;
			NetworkRegistry.TargetPoint point = new NetworkRegistry.TargetPoint(worldObj.provider.getDimensionId(), pos.getX(), pos.getY(), pos.getZ(), 16.0d);
			JCMain.instance.proxy.networkController.network.sendToAllAround(new PacketCampFire(pos,this.getField(0),this.getField(1), this.getField(2),this.getField(3)), point);
		}
		
		//[ 1 = burn-able, 3 = utensil, 0 = food, 2 = output ]
		
		if(!this.worldObj.isRemote){
			// Count down cook time if not burning and no item in either food or burn slot
			if(!this.isBurning() && (this.campFireInventory[1] == null || this.campFireInventory[0] == null)){
				if(!this.isBurning() && this.cookTime > 0){
					this.cookTime = MathHelper.clamp_int(this.cookTime -2, 0, this.totalCookTime);
				}
			}else{
				// Start cooking
				if(!this.isBurning() && this.canCook()){
					// Set current item burn time and camp fire burn time to burnable items burn time.
					this.currentItemBurnTime = this.furnaceBurnTime = getItemBurnTime(this.campFireInventory[1]);
					
					// because we set camp fire burn time bigger than zero - this DOES get called.  a mazing.
					if(this.isBurning()){
						needMarkDirty = true;
						
						// If we have stuff in burnable slot - remove one of those burnable things.
						
						if(this.campFireInventory[1] != null){
							--this.campFireInventory[1].stackSize;
						
							
							// Return bucket or container of item if item has container.
							if(this.campFireInventory[1].stackSize == 0){
								this.campFireInventory[1] = campFireInventory[1].getItem().getContainerItem(campFireInventory[1]);
							}
						}
					}
				}
				
				if(this.isBurning() && this.canCook()){
					// Increment the cook time
					++this.cookTime;
					
					if(this.cookTime == this.totalCookTime){
						this.cookTime = 0;
						// Set total cook time to the foods cook time
						this.totalCookTime = this.getCookTime(this.campFireInventory[0]);
						this.cookItem();
						needMarkDirty = true;
					}
				}else{
					this.cookTime = 0;
				}
			}
			
			if(burning != this.isBurning()){
				needMarkDirty = true;
				// SET BLOCK STATE OF CAMP FIRE BlockFurnace.setState(this.isburning(), this world, this pos)
			}
				
		}
		
		if(needMarkDirty){
			this.markDirty();
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		
		NBTTagList list = new NBTTagList();
		for(int i = 0; i<this.getSizeInventory(); ++i){
			if(this.getStackInSlot(i) != null){
				NBTTagCompound stackTag = new NBTTagCompound();
				stackTag.setByte("Slot", (byte)i);
				this.getStackInSlot(i).writeToNBT(stackTag);
				list.appendTag(stackTag);
			}
		}
		nbt.setTag("Items", list);
		
		if(this.hasCustomName()){
			nbt.setString("CustomName", this.getCustomName());
		}
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		
		NBTTagList list = nbt.getTagList("Items", 10);
		for(int i = 0; i<list.tagCount(); ++i){
			NBTTagCompound stackTag = list.getCompoundTagAt(i);
			int slot = stackTag.getByte("Slot") & 255;
			this.setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(stackTag));
		}
		
		if(nbt.hasKey("CustomName", 8)){
			this.setCustomName(nbt.getString("CustomName"));
		}
	}
	
	public String getCustomName(){
		return this.customName;
	}

	private void cookItem() {
		//[ 1 = burn-able, 3 = utensil, 0 = food, 2 = output ]
		// Check if item is cookable and continue.
		if (this.canCook())
        {
			ItemStack itemstack = CampFireRecipes.instance().getCookingResult(new JCRecipeBuddy(this.campFireInventory[0], this.campFireInventory[3]));
           // ItemStack itemstack = FurnaceRecipes.instance().getSmeltingResult(this.campFireInventory[0]);

            // if we have nothing in output, copy recipe result into output field.
            if (this.campFireInventory[2] == null)
            {
                this.campFireInventory[2] = itemstack.copy();
            }	// if the recipe output item is the same as the item currently in out, just add teh stack size to teh item stack
            else if (this.campFireInventory[2].getItem() == itemstack.getItem())
            {
                this.campFireInventory[2].stackSize += itemstack.stackSize; // Forge BugFix: Results may have multiple items
            }
            
            // Give water bucket from wet sponge --- wtf
            if (this.campFireInventory[0].getItem() == Item.getItemFromBlock(Blocks.sponge) && this.campFireInventory[0].getMetadata() == 1 && this.campFireInventory[1] != null && this.campFireInventory[1].getItem() == Items.bucket)
            {
                this.campFireInventory[1] = new ItemStack(Items.water_bucket);
            }

            // Decrease the stack size of the food
            --this.campFireInventory[0].stackSize;
            --this.campFireInventory[3].stackSize;
            
            if (this.campFireInventory[3].stackSize <= 0)
            {
                this.campFireInventory[3] = null;
            }
        	
            if (this.campFireInventory[0].stackSize <= 0)
            {
                this.campFireInventory[0] = null;
            }
        }
	}

	private int getCookTime(ItemStack itemStack) {
		// If we want some items to take longer to cook, this is where we set that.
		return 200;
	}

	private int getItemBurnTime(ItemStack itemStack) {
		if (itemStack == null)
        {
            return 0;
        }
        else
        {
            Item item = itemStack.getItem();

            if (item instanceof ItemBlock && Block.getBlockFromItem(item) != Blocks.air)
            {
                Block block = Block.getBlockFromItem(item);

                if (block == Blocks.wooden_slab) //Camp fire can only use "wood" items as fuel. Don't want food tasting like coal.
                {
                    return 150;
                }

                if (block.getMaterial() == Material.wood)
                {
                    return 300;
                }
                
            }

            if (item instanceof ItemTool && ((ItemTool)item).getToolMaterialName().equals("WOOD")) return 200;
            if (item instanceof ItemSword && ((ItemSword)item).getToolMaterialName().equals("WOOD")) return 200;
            if (item instanceof ItemHoe && ((ItemHoe)item).getMaterialName().equals("WOOD")) return 200;
            if (item == Items.stick) return 100;
            if (item == Items.coal) return 1600;
            if (item == Items.lava_bucket) return 20000;
            if (item == Item.getItemFromBlock(Blocks.sapling)) return 100;
            if (item == Items.blaze_rod) return 2400;
            return net.minecraftforge.fml.common.registry.GameRegistry.getFuelValue(itemStack);
        }
	}

	private boolean canCook() {
		
		// this is where we determine if the item is cookable ... so this is where we check if the item is food.
		
		if (this.campFireInventory[0] == null)
        {
            return false;
        }
        else
        {
        	// Use our recipe class here ----- WOOOOOOOOOOOOT
        	ItemStack itemstack = CampFireRecipes.instance().getCookingResult(new JCRecipeBuddy(this.campFireInventory[0], this.campFireInventory[3]));
            //ItemStack itemstack = FurnaceRecipes.instance().getSmeltingResult(this.campFireInventory[0]);
            if (itemstack == null) return false;
            if (this.campFireInventory[2] == null) return true;
            if (!this.campFireInventory[2].isItemEqual(itemstack)) return false;
            int result = campFireInventory[2].stackSize + itemstack.stackSize;
            return result <= getInventoryStackLimit() && result <= this.campFireInventory[2].getMaxStackSize(); //Forge BugFix: Make it respect stack sizes properly.
        }
	}

	public boolean isBurning(){
		return this.furnaceBurnTime > 0;
	}

	@SideOnly(Side.CLIENT)
	public static boolean isBurning(IInventory ceInv) {
		return ceInv.getField(0) > 0;
	}
}