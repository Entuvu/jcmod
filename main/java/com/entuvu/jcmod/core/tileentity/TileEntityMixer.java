package com.entuvu.jcmod.core.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.IChatComponent;

public class TileEntityMixer extends TileEntity implements IInventory {

	private static final int CONTAINER = 0;
	private static final int BL = 1;
	private static final int BR = 2;
	private static final int TL = 3;
	private static final int TR = 4;
	private static final int OUTPUT = 5;
	
	private ItemStack[] mixerInventory;
	private String customName;

	public TileEntityMixer() {
		this.mixerInventory = new ItemStack[this.getSizeInventory()];
	}

	@Override
	public String getName() {
		return this.hasCustomName() ? this.customName : "container.jcmod_mixer";		
	}

	@Override
	public boolean hasCustomName() {
		return this.customName != null && !this.customName.equals("");
	}

	@Override
	public IChatComponent getDisplayName() {
		return this.hasCustomName() ? new ChatComponentText(this.getName()) : new ChatComponentTranslation(this.getName());
	}

	@Override
	public int getSizeInventory() {
		return 6;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		if(index < 0 || index >= this.getSizeInventory())
			return null;
		return this.mixerInventory[index];
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		if(this.getStackInSlot(index) != null){
			ItemStack itemstack;
			if(this.getStackInSlot(index).stackSize <= count){
				itemstack = this.getStackInSlot(index);
				this.setInventorySlotContents(index, null);
				this.markDirty();
				return itemstack;
			}else{
				itemstack = this.getStackInSlot(index).splitStack(count);
				if(this.getStackInSlot(index).stackSize<= 0){
					this.setInventorySlotContents(index, null);
				}else{
					// Just to show that changes happened
					this.setInventorySlotContents(index, this.getStackInSlot(index));
				}
				
				this.markDirty();
				return itemstack;
			}
		}else{
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int index) {
		ItemStack stack = this.getStackInSlot(index);
		this.setInventorySlotContents(index, null);
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		boolean flag = stack != null && stack.isItemEqual(this.mixerInventory[index]) && ItemStack.areItemStackTagsEqual(stack, this.mixerInventory[index]);
        this.mixerInventory[index] = stack;

        if (stack != null && stack.stackSize > this.getInventoryStackLimit())
        {
            stack.stackSize = this.getInventoryStackLimit();
        }

        if (index == 0 && !flag)
        {
            this.markDirty();
        }
		
	}

	@Override
	public int getInventoryStackLimit() {
		// TODO Auto-generated method stub
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.getPos()) == this && player.getDistanceSq(this.pos.add(0.5,0.5,0.5)) <= 64;
	}

	@Override
	public void openInventory(EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeInventory(EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int getField(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setField(int id, int value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getFieldCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void clear() {
		for(int i = 0; i<this.getSizeInventory(); i++)
		this.setInventorySlotContents(i, null);		
	}
	public void setCustomName(String displayName) {
		this.customName = displayName;		
	}
	

}
