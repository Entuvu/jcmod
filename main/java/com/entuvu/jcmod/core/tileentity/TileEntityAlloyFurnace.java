package com.entuvu.jcmod.core.tileentity;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.crafting.AlloyFurnaceRecipes;
import com.entuvu.jcmod.core.crafting.JCRecipeBuddy;
import com.entuvu.jcmod.core.network.PacketAlloyFurnace;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.gui.IUpdatePlayerListBox;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.MathHelper;
import net.minecraftforge.fml.common.network.NetworkRegistry;


public class TileEntityAlloyFurnace extends TileEntity implements IInventory, IUpdatePlayerListBox {

	public static final int LEFT_FUEL = 0;
	public static final int LEFT_ALLOY = 1;
	public static final int RIGHT_FUEL = 2;
	public static final int RIGHT_ALLOY = 3;
	public static final int OUTPUT = 4;
	
	private String customName;
	private ItemStack[] alloyFurnaceInventory;
	
	private int totalSmeltingTime = 200;
	private int currentSmeltingTime;
	
	private int	leftBurnTime;
	private int rightBurnTime;
	
	private int leftItemBurnTime;
	private int rightItemBurnTime;
	
	public TileEntityAlloyFurnace(){
		/* This Constructor sets the Inventory Array to the proper size */
		this.alloyFurnaceInventory = new ItemStack[this.getSizeInventory()];
	}

	@Override
	public String getName() {
		/* Returns the custom name if present, if not, returns general entity name */
		return this.hasCustomName() ? this.customName : "container.jcmod_alloy_furnace";
	}

	@Override
	public boolean hasCustomName() {
		/* Returns true if a Custom Name is set */
		return this.customName != null && !this.customName.equals("");
	}

	@Override
	public IChatComponent getDisplayName() {
		return this.hasCustomName() ? new ChatComponentText(this.getName()) : new ChatComponentTranslation(this.getName());
	}

	@Override
	public int getSizeInventory() {
		/* This needs to be the number of slots the whole inventory has. */
		return 5;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		/* This returns a stack in a slot if it is not out of range. */
		if(index < 0 || index >= this.getSizeInventory())
			return null;
		return this.alloyFurnaceInventory[index];
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		if(this.getStackInSlot(index) != null){
			ItemStack itemstack;
			if(this.getStackInSlot(index).stackSize <= count){
				itemstack = this.getStackInSlot(index);
				this.setInventorySlotContents(index, null);
				this.markDirty();
				return itemstack;
			}else{
				itemstack = this.getStackInSlot(index).splitStack(count);
				if(this.getStackInSlot(index).stackSize<= 0){
					this.setInventorySlotContents(index, null);
				}else{
					// Just to show that changes happened
					this.setInventorySlotContents(index, this.getStackInSlot(index));
				}
				
				this.markDirty();
				return itemstack;
			}
		}else{
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int index) {
		ItemStack stack = this.getStackInSlot(index);
		this.setInventorySlotContents(index, null);
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		boolean flag = stack != null && stack.isItemEqual(this.alloyFurnaceInventory[index]) && ItemStack.areItemStackTagsEqual(stack, this.alloyFurnaceInventory[index]);
        this.alloyFurnaceInventory[index] = stack;

        if (stack != null && stack.stackSize > this.getInventoryStackLimit())
        {
            stack.stackSize = this.getInventoryStackLimit();
        }

        if (index == 0 && !flag)
        {
        	// This is important - this sets the total cook time to the cook time of the item and resets the cook time to zero
        	// When an item is placed in the slot.
            
        	//this.totalCookTime = this.getCookTime(stack);
            this.rightBurnTime = 0;
            this.leftBurnTime = 0;
            this.markDirty();
        }
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.getPos()) == this && player.getDistanceSq(this.pos.add(0.5,0.5,0.5)) <= 64;
	}

	@Override
	public void openInventory(EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeInventory(EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		 return index == 2 ? false : (index != 1 ? true : isItemFuel(stack) || SlotFurnaceFuel.isBucket(stack));
	}
	
	private boolean isItemFuel(ItemStack stack) {
		return getItemBurnTime(stack)>0;
	}

	@Override
	 public int getField(int id)
   {
       switch (id)
       {
           case 0:
               return this.leftBurnTime;
           case 1:
               return this.leftItemBurnTime;
           case 2:
               return this.rightBurnTime;
           case 3:
               return this.rightItemBurnTime;
           case 4:
               return this.currentSmeltingTime;
           case 5:
               return this.totalSmeltingTime;
           default:
               return 0;
       }
   }
	
	@Override
   public void setField(int id, int value)
   {
       switch (id)
       {
           case 0:
               this.leftBurnTime = value;
               break;
           case 1:
               this.leftItemBurnTime = value;
               break;
           case 2:
               this.rightBurnTime = value;
               break;
           case 3:
               this.rightItemBurnTime = value;
               break;
           case 4:
               this.currentSmeltingTime = value;
               break;
           case 5:
               this.totalSmeltingTime = value;
               break;
       }
   }
	
	@Override
   public int getFieldCount()
   {
       return 6;
   }

	@Override
	public void clear() {
		for(int i = 0; i<this.getSizeInventory(); i++)
			this.setInventorySlotContents(i, null);
	}

	public void setCustomName(String customName){
		this.customName = customName;
	}

	@Override
	public void update() {
		
		boolean markItDirty = false;
		boolean burning = this.isBurning();
		
		if(this.isBurning()){
			NetworkRegistry.TargetPoint point = new NetworkRegistry.TargetPoint(worldObj.provider.getDimensionId(), pos.getX(), pos.getY(), pos.getZ(), 16.0d);
			JCMain.instance.proxy.networkController.network.sendToAllAround(new PacketAlloyFurnace(pos,this.getField(0),this.getField(1), this.getField(2),this.getField(3),this.getField(4),this.getField(5)), point);
		}
		
		
		/* Alloy Furnace 0-4 ...... 0  - 4  [ 0 = burn-able-1, 1 = alloy-1, 2 = burn-able-2, 3 = alloy-2, 4 = output ] */
		if(!this.worldObj.isRemote){
			// tick down burn
			if(this.isBurning()){
				--this.leftBurnTime;
				--this.rightBurnTime;
			}
			
			if(!this.isBurning()  &&  (this.alloyFurnaceInventory[LEFT_FUEL] == null || this.alloyFurnaceInventory[RIGHT_FUEL] == null || this.alloyFurnaceInventory[RIGHT_ALLOY] == null || this.alloyFurnaceInventory[LEFT_ALLOY] == null)){
				if(this.currentSmeltingTime > 0){
					this.currentSmeltingTime = MathHelper.clamp_int(currentSmeltingTime -2, 0, this.totalSmeltingTime);
				}
			}else{
				/* It is burning and the four required spots are not empty */
				
				
				if(!this.isBurning() && this.canSmelt()){
					
					this.leftItemBurnTime = this.leftBurnTime = this.getItemBurnTime(this.alloyFurnaceInventory[LEFT_FUEL]);
					this.rightItemBurnTime = this.rightBurnTime = this.getItemBurnTime(this.alloyFurnaceInventory[RIGHT_FUEL]);
					
					if(this.isBurning()){
						markItDirty = true;
					}
					
					if(this.alloyFurnaceInventory[LEFT_FUEL] != null){
						--this.alloyFurnaceInventory[LEFT_FUEL].stackSize;
						
						// Return bucket or container of item if item has container.
						if(this.alloyFurnaceInventory[LEFT_FUEL].stackSize == 0){
							this.alloyFurnaceInventory[LEFT_FUEL] = alloyFurnaceInventory[LEFT_FUEL].getItem().getContainerItem(alloyFurnaceInventory[LEFT_FUEL]);
						}
					}
					
					if(this.alloyFurnaceInventory[RIGHT_FUEL] != null){
						--this.alloyFurnaceInventory[RIGHT_FUEL].stackSize;
						
						// Return bucket or container of item if item has container.
						if(this.alloyFurnaceInventory[RIGHT_FUEL].stackSize == 0){
							this.alloyFurnaceInventory[RIGHT_FUEL] = alloyFurnaceInventory[RIGHT_FUEL].getItem().getContainerItem(alloyFurnaceInventory[RIGHT_FUEL]);
						}
					}
					
				}
				
				if(this.isBurning() && this.canSmelt()){
					++this.currentSmeltingTime;
					if(this.currentSmeltingTime >= this.totalSmeltingTime){
						this.currentSmeltingTime = 0;
						this.totalSmeltingTime = 200;
						this.smeltItem();
						markItDirty = true;
					}
				}else{
					this.currentSmeltingTime = 0;
				}
			}
			
			if(burning != this.isBurning()){
				markItDirty = true;
			}
		}
		
		if(markItDirty)
			this.markDirty();
	}

	private void smeltItem() {
		if (this.canSmelt())
	    {
			ItemStack itemstack = AlloyFurnaceRecipes.instance().getSmeltingResult(new JCRecipeBuddy(this.alloyFurnaceInventory[LEFT_ALLOY], this.alloyFurnaceInventory[RIGHT_ALLOY]));
	       // ItemStack itemstack = FurnaceRecipes.instance().getSmeltingResult(this.campFireInventory[0]);
	
	        // if we have nothing in output, copy recipe result into output field.
	        if (this.alloyFurnaceInventory[OUTPUT] == null)
	        {
	            this.alloyFurnaceInventory[OUTPUT] = itemstack.copy();
	        }	// if the recipe output item is the same as the item currently in out, just add the stack size to the item stack
	        else if (this.alloyFurnaceInventory[OUTPUT].getItem() == itemstack.getItem())
	        {
	            this.alloyFurnaceInventory[OUTPUT].stackSize += itemstack.stackSize; // Forge BugFix: Results may have multiple items
	        }
	
	        // Decrease the stack size of the alloy
	        --this.alloyFurnaceInventory[LEFT_ALLOY].stackSize;
	        --this.alloyFurnaceInventory[RIGHT_ALLOY].stackSize;
	    	
	        if (this.alloyFurnaceInventory[LEFT_ALLOY].stackSize <= 0)
	        {
	            this.alloyFurnaceInventory[LEFT_ALLOY] = null;
	        }
	        
	        if (this.alloyFurnaceInventory[RIGHT_ALLOY].stackSize <= 0)
	        {
	            this.alloyFurnaceInventory[RIGHT_ALLOY] = null;
	        }
	        
	    }
	}

	private boolean canSmelt() {
		
		if(this.alloyFurnaceInventory[LEFT_ALLOY] == null || this.alloyFurnaceInventory[RIGHT_ALLOY] == null){
			return false;
		}else{
			
			ItemStack itemstack = AlloyFurnaceRecipes.instance().getSmeltingResult(new JCRecipeBuddy(this.alloyFurnaceInventory[LEFT_ALLOY], this.alloyFurnaceInventory[RIGHT_ALLOY]));
			if (itemstack == null) return false;
            if (this.alloyFurnaceInventory[4] == null) return true;
            if (!this.alloyFurnaceInventory[4].isItemEqual(itemstack)) return false;
            int result = alloyFurnaceInventory[4].stackSize + itemstack.stackSize;
            return result <= getInventoryStackLimit() && result <= this.alloyFurnaceInventory[4].getMaxStackSize(); //Forge BugFix: Make it respect stack sizes properly.
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		
		NBTTagList list = new NBTTagList();
		for(int i = 0; i<this.getSizeInventory(); ++i){
			if(this.getStackInSlot(i) != null){
				NBTTagCompound stackTag = new NBTTagCompound();
				stackTag.setByte("Slot", (byte)i);
				this.getStackInSlot(i).writeToNBT(stackTag);
				list.appendTag(stackTag);
			}
		}
		nbt.setTag("Items", list);
		
		if(this.hasCustomName()){
			nbt.setString("CustomName", this.getCustomName());
		}
		
		/* Add smelting times to this later */
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		
		NBTTagList list = nbt.getTagList("Items", 10);
		for(int i = 0; i<list.tagCount(); ++i){
			NBTTagCompound stackTag = list.getCompoundTagAt(i);
			int slot = stackTag.getByte("Slot") & 255;
			this.setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(stackTag));
		}
		
		if(nbt.hasKey("CustomName", 8)){
			this.setCustomName(nbt.getString("CustomName"));
		}
		
		/* Add smelting times to this later */
	}
	
	private String getCustomName() {
		return this.customName;
	}
	
	private int getItemBurnTime(ItemStack itemStack){
		if (itemStack == null)
        {
            return 0;
        }
        else
        {
            Item item = itemStack.getItem();

            if (item instanceof ItemBlock && Block.getBlockFromItem(item) != Blocks.air)
            {
                Block block = Block.getBlockFromItem(item);

                if (block == Blocks.coal_block) //Alloy Furnace only can use coal as a fuel
                {
                    return 16000;
                }
            }

            if (item instanceof ItemTool && ((ItemTool)item).getToolMaterialName().equals("WOOD")) return 200;
            if (item instanceof ItemSword && ((ItemSword)item).getToolMaterialName().equals("WOOD")) return 200;
            if (item instanceof ItemHoe && ((ItemHoe)item).getMaterialName().equals("WOOD")) return 200;
            if (item == Items.stick) return 100;
            if (item == Items.coal) return 1600;
            if (item == Items.lava_bucket) return 20000;
            if (item == Item.getItemFromBlock(Blocks.sapling)) return 100;
            if (item == Items.blaze_rod) return 2400;
            return net.minecraftforge.fml.common.registry.GameRegistry.getFuelValue(itemStack);
        }
	}
	
	public boolean isBurning(){
		return this.leftBurnTime > 0 && this.rightBurnTime > 0;
	}

	public static boolean isBurning(IInventory alloyFurnaceInventory2) {
		return alloyFurnaceInventory2.getField(0) > 0 && alloyFurnaceInventory2.getField(2) > 0;
	}
	
	public static boolean isBurning(IInventory alloyFurnaceInv, int field){
		return alloyFurnaceInv.getField(field) > 0;
	}
}
