package com.entuvu.jcmod.core.tileentity;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.crafting.JCRecipeBuddy;
import com.entuvu.jcmod.core.crafting.SmeltingFurnaceRecipes;
import com.entuvu.jcmod.core.network.PacketSmeltingFurnace;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.gui.IUpdatePlayerListBox;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.MathHelper;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


public class TileEntitySmeltingFurnace extends TileEntity implements IInventory, IUpdatePlayerListBox {

	public static int CHUNK1 = 0;
	public static int CHUNK2 = 1;
	public static int CAST = 2;
	public static int BURNABLE = 3;
	public static int OUTPUT = 4;
	
	private String customName;
	private ItemStack[] smeltingFurnaceInventory;
	
	private int furnaceBurnTime;
	private int currentItemBurnTime;
	
	private int totalCookTime;
	private int currentCookTime;
	
	public TileEntitySmeltingFurnace(){
		this.smeltingFurnaceInventory = new ItemStack[this.getSizeInventory()];
	}
	
	@Override
	public String getName() {
		return this.hasCustomName() ? this.customName : "container.smelting_furnace";
	}

	@Override
	public boolean hasCustomName() {
		return this.customName != null && !this.customName.equals("");
	}

	@Override
	public IChatComponent getDisplayName() {
		return this.hasCustomName() ? new ChatComponentText(this.getName()):new ChatComponentTranslation(this.getName());
	}

	@Override
	public void update() {
		//
		
		boolean markItDirty = false;
		boolean burning = this.isBurning();
		
		if(this.isBurning()){
			
			--this.furnaceBurnTime;
			NetworkRegistry.TargetPoint point = new NetworkRegistry.TargetPoint(worldObj.provider.getDimensionId(), pos.getX(), pos.getY(), pos.getZ(), 16.0d);
			JCMain.instance.proxy.networkController.network.sendToAllAround(new PacketSmeltingFurnace(pos,this.getField(0),this.getField(1), this.getField(2),this.getField(3)), point);
			
		}
		
		if(!this.worldObj.isRemote){
			
			if(!this.isBurning() && 
					(this.smeltingFurnaceInventory[BURNABLE] == null || 
					this.smeltingFurnaceInventory[CHUNK1] == null ||
					this.smeltingFurnaceInventory[CHUNK2] == null)){
				
				if(!this.isBurning() && this.currentCookTime > 0){
					this.currentCookTime = MathHelper.clamp_int(this.currentCookTime - 2, 0, this.totalCookTime);
				}
			}else{
				if(!this.isBurning() && this.canSmelt()){
					this.currentItemBurnTime = this.furnaceBurnTime = getItemBurnTime(this.smeltingFurnaceInventory[BURNABLE]);
					
					if(this.isBurning()){
						burning = true;
						
						if(this.smeltingFurnaceInventory[BURNABLE] != null ){
							--this.smeltingFurnaceInventory[BURNABLE].stackSize;
							
							if(this.smeltingFurnaceInventory[BURNABLE].stackSize == 0){
								this.smeltingFurnaceInventory[BURNABLE] = smeltingFurnaceInventory[BURNABLE].getItem().getContainerItem(smeltingFurnaceInventory[BURNABLE]);
							}
						}
					}
				}
			}
			
			if(this.isBurning() && this.canSmelt()){
				++this.currentCookTime;
				
				if(this.currentCookTime == this.totalCookTime){
					this.currentCookTime = 0;
					this.totalCookTime = this.getTotalCookTime(this.smeltingFurnaceInventory[CHUNK1]);
					this.smeltItem();
					markItDirty=true;
				}
			}else{
				this.currentCookTime = 0;
				
			}
		}
	}

	public void smeltItem() {
		if(this.canSmelt()){
			ItemStack itemstack = SmeltingFurnaceRecipes.instance().getSmeltingResult(new JCRecipeBuddy(this.smeltingFurnaceInventory[CAST],this.smeltingFurnaceInventory[CHUNK1]));
			
			if(this.smeltingFurnaceInventory[OUTPUT] == null){
				this.smeltingFurnaceInventory[OUTPUT] = itemstack.copy();
			}
			else if (this.smeltingFurnaceInventory[OUTPUT].getItem() == itemstack.getItem()){
				this.smeltingFurnaceInventory[OUTPUT].stackSize += itemstack.stackSize;
			}
			
			--this.smeltingFurnaceInventory[CHUNK1].stackSize;
			--this.smeltingFurnaceInventory[CHUNK2].stackSize;
			
			if(this.smeltingFurnaceInventory[CHUNK1].stackSize <= 0){
				this.smeltingFurnaceInventory[CHUNK1] = null;
			}
			
			if(this.smeltingFurnaceInventory[CHUNK2].stackSize <= 0){
				this.smeltingFurnaceInventory[CHUNK2] = null;
			}
		}
	}

	private boolean canSmelt() {
		if(this.smeltingFurnaceInventory[CHUNK1] == null || 
				this.smeltingFurnaceInventory[CHUNK2] == null ||
				this.smeltingFurnaceInventory[CHUNK1].getItem() != 
				this.smeltingFurnaceInventory[CHUNK2].getItem()){
			
			return false;
		}else{
			
			ItemStack itemstack = SmeltingFurnaceRecipes.instance().getSmeltingResult(new JCRecipeBuddy(this.smeltingFurnaceInventory[CAST], this.smeltingFurnaceInventory[CHUNK1]));
			if(itemstack == null) return false;
			if(this.smeltingFurnaceInventory[OUTPUT] == null) return true;
			if(!this.smeltingFurnaceInventory[OUTPUT].isItemEqual(itemstack)) return false;
			int result = smeltingFurnaceInventory[OUTPUT].stackSize + itemstack.stackSize;
			return result <= getInventoryStackLimit() && result <= this.smeltingFurnaceInventory[OUTPUT].getMaxStackSize();
		}
	}

	public boolean isBurning() {
		return this.furnaceBurnTime > 0;
	}

	@Override
	public int getSizeInventory() {
		return 5;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		/* This returns a stack in a slot if it is not out of range. */
		if(index < 0 || index >= this.getSizeInventory())
			return null;
		return this.smeltingFurnaceInventory[index];
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		if(this.getStackInSlot(index) != null){
			ItemStack itemstack;
			if(this.getStackInSlot(index).stackSize <= count){
				itemstack = this.getStackInSlot(index);
				this.setInventorySlotContents(index, null);
				this.markDirty();
				return itemstack;
			}else{
				itemstack = this.getStackInSlot(index).splitStack(count);
				if(this.getStackInSlot(index).stackSize<= 0){
					this.setInventorySlotContents(index, null);
				}else{
					// Just to show that changes happened
					this.setInventorySlotContents(index, this.getStackInSlot(index));
				}
				
				this.markDirty();
				return itemstack;
			}
		}else{
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int index) {
		ItemStack stack = this.getStackInSlot(index);
		this.setInventorySlotContents(index, null);
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		// Looks like this is called when an inventory item is put into a slot, any slot. //
		
		boolean flag = stack != null && stack.isItemEqual(this.smeltingFurnaceInventory[index]) && ItemStack.areItemStackTagsEqual(stack, this.smeltingFurnaceInventory[index]);
        this.smeltingFurnaceInventory[index] = stack;

        if (stack != null && stack.stackSize > this.getInventoryStackLimit())
        {
            stack.stackSize = this.getInventoryStackLimit();
        }
        
        if (index == BURNABLE && !flag)
        {
        	this.totalCookTime = this.getTotalCookTime(stack);
        	this.currentCookTime = 0;
            this.markDirty();
        }
	}

	public int getTotalCookTime(ItemStack stack) {
		return 200; /// Same as vanilla furnace for now //
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.getPos()) == this && player.getDistanceSq(this.pos.add(0.5,0.5,0.5)) <= 64;
	}

	@Override
	public void openInventory(EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeInventory(EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		if(index == BURNABLE){
			if(isItemFuel(stack) || SlotFurnaceFuel.isBucket(stack)){
				return true;
			}
			return false;
		}
		
		return true;
	}

	public static boolean isItemFuel(ItemStack stack) {
		return getItemBurnTime(stack)>0;
	}

	public static int getItemBurnTime(ItemStack itemStack){
		if (itemStack == null)
        {
            return 0;
        }
        else
        {
            Item item = itemStack.getItem();

            if (item instanceof ItemBlock && Block.getBlockFromItem(item) != Blocks.air)
            {
                Block block = Block.getBlockFromItem(item);

                if (block == Blocks.coal_block) //Alloy Furnace only can use coal as a fuel
                {
                    return 16000;
                }
            }

            if (item instanceof ItemTool && ((ItemTool)item).getToolMaterialName().equals("WOOD")) return 200;
            if (item instanceof ItemSword && ((ItemSword)item).getToolMaterialName().equals("WOOD")) return 200;
            if (item instanceof ItemHoe && ((ItemHoe)item).getMaterialName().equals("WOOD")) return 200;
            if (item == Items.stick) return 100;
            if (item == Items.coal) return 1600;
            if (item == Items.lava_bucket) return 20000;
            if (item == Item.getItemFromBlock(Blocks.sapling)) return 100;
            if (item == Items.blaze_rod) return 2400;
            return net.minecraftforge.fml.common.registry.GameRegistry.getFuelValue(itemStack);
        }
	}

	@Override
	public int getField(int id) {
		switch (id)
        {
            case 0:
                return this.furnaceBurnTime;
            case 1:
                return this.currentItemBurnTime;
            case 2:
                return this.currentCookTime;
            case 3:
                return this.totalCookTime;
            default:
                return 0;
        }
	}

	@Override
    public void setField(int id, int value)
    {
        switch (id)
        {
            case 0:
                this.furnaceBurnTime = value;
                break;
            case 1:
                this.currentItemBurnTime = value;
                break;
            case 2:
                this.currentCookTime = value;
                break;
            case 3:
                this.totalCookTime = value;
        }
    }

	@Override
	public int getFieldCount() {
		 return 4;
	}

	@Override
	public void clear() {
		for(int i = 0; i<this.getSizeInventory(); i++)
			this.setInventorySlotContents(i, null);
	}

	public void setCustomName(String displayName) {
		this.customName = displayName;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        compound.setShort("BurnTime", (short)this.furnaceBurnTime);
        compound.setShort("CookTime", (short)this.currentCookTime);
        compound.setShort("CookTimeTotal", (short)this.totalCookTime);
        NBTTagList nbttaglist = new NBTTagList();

        for (int i = 0; i < this.smeltingFurnaceInventory.length; ++i)
        {
            if (this.smeltingFurnaceInventory[i] != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                this.smeltingFurnaceInventory[i].writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        compound.setTag("Items", nbttaglist);

        if (this.hasCustomName())
        {
            compound.setString("CustomName", this.customName);
        }
    }

	@Override
	public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        NBTTagList nbttaglist = compound.getTagList("Items", 10);
        this.smeltingFurnaceInventory = new ItemStack[this.getSizeInventory()];

        for (int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
            byte b0 = nbttagcompound1.getByte("Slot");

            if (b0 >= 0 && b0 < this.smeltingFurnaceInventory.length)
            {
                this.smeltingFurnaceInventory[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
            }
        }

        this.furnaceBurnTime = compound.getShort("BurnTime");
        this.currentCookTime = compound.getShort("CookTime");
        this.totalCookTime = compound.getShort("CookTimeTotal");
        this.currentItemBurnTime = getItemBurnTime(this.smeltingFurnaceInventory[1]);

        if (compound.hasKey("CustomName", 8))
        {
            this.customName = compound.getString("CustomName");
        }
    }
	
	public String getCustomName() {
		return this.customName;
	}

	@SideOnly(Side.CLIENT)
	public static boolean isBurning(IInventory tileEntity) {
		return tileEntity.getField(0) > 0;
	}

}
