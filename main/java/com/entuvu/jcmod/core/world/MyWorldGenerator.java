package com.entuvu.jcmod.core.world;

import java.util.Random;

import com.entuvu.jcmod.core.register.RegisterBlock;

import net.minecraft.block.BlockCrops;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.fml.common.IWorldGenerator;

public class MyWorldGenerator implements IWorldGenerator {
	
	private WorldGenerator gen_copper_ore; 	// Generates copper ore
	private WorldGenerator gen_tin_ore; 	// Generates tin ore
	private WorldGenerator gen_lead_ore;	// Generates lead ore
	private WorldGenerator gen_silver_ore;	// Generates silver ore
	private WorldGenerator jute_generator;	// Generates jute plant
	
	public MyWorldGenerator(){
		this.gen_copper_ore = new WorldGenMinable(RegisterBlock.copper_ore.getDefaultState(), 8);
		this.gen_tin_ore = new WorldGenMinable(RegisterBlock.tin_ore.getDefaultState(), 8);
		this.gen_lead_ore = new WorldGenMinable(RegisterBlock.lead_ore.getDefaultState(), 8);
		this.gen_silver_ore = new WorldGenMinable(RegisterBlock.silver_ore.getDefaultState(), 8);
		/* Plant Generation --------------------- Block set as default state with properties [age here] ----------------------------- grouping, chance of spawning ---- comma list of biomes types it can spawn in */
		this.jute_generator = new WorldGenPlants(RegisterBlock.jute_plant.getDefaultState().withProperty(BlockCrops.AGE, Integer.valueOf(7)), 20, 20, BiomeDictionary.Type.FOREST, BiomeDictionary.Type.PLAINS);
	}

	@Override
	public void generate(Random rand, int chunk_X, int chunk_Z, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		switch(world.provider.getDimensionId()){
		case 0: // Overworld
			/* Ores */
			this.runGenerator(this.gen_copper_ore, world, rand, chunk_X, chunk_Z, 20, 0, 64);
			this.runGenerator(this.gen_tin_ore, world, rand, chunk_X, chunk_Z, 20, 0, 128);
			this.runGenerator(this.gen_lead_ore, world, rand, chunk_X, chunk_Z, 20, 20, 40);
			this.runGenerator(this.gen_silver_ore, world, rand, chunk_X, chunk_Z, 10, 10, 40);
			/* Plants */
			this.runGenerator(this.jute_generator, world, rand, chunk_X, chunk_Z, 10, 50, 100);
			break;
		case -1: // Nether
			
			break;
			
		case 1: // End World
			
			break;
		}
	}
	
	private void runGenerator(WorldGenerator generator, World world, Random rand, int chunk_X, int chunk_Z, int chancesToSpawn, int minHeight, int maxHeight){
		if(minHeight < 0 || maxHeight > 256 || minHeight > maxHeight)
			throw new IllegalArgumentException("Illegal Height Arguments for WorldGenerator");
		
		int heightDiff = maxHeight - minHeight + 1;
		for(int i = 0; i < chancesToSpawn; i ++){
			int x = chunk_X * 16 + rand.nextInt(16);
			int y = minHeight + rand.nextInt(heightDiff);
			int z = chunk_Z * 16 + rand.nextInt(16);
			generator.generate(world, rand, new BlockPos(x,y,z));
		}
	}
	
	
}
