package com.entuvu.jcmod.core.blocks;

import java.util.Random;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.event.DropSpecial;
import com.entuvu.jcmod.core.register.RegisterItem;

import net.minecraft.block.Block;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

public class BlockJute extends BlockCrops {
	public BlockJute(String string) {
		this.setUnlocalizedName(string);
		this.setCreativeTab(JCMain.tabStandard);
	}

	@Override
	protected Item getSeed() {
		return null;
	}
	
	@Override
	protected Item getCrop() {
		return null;
	}
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		return null;
	}
	
	@Override
	public boolean removedByPlayer(World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
		if(!world.isRemote){
			if(((Integer) world.getBlockState(pos).getValue(AGE)).intValue() == 7){
				DropSpecial.drop(1, 1, RegisterItem.jute_seed, -100, pos, world);
				DropSpecial.drop(1, 2, RegisterItem.jute, -100, pos, world);
			}else{
				DropSpecial.drop(1, 0, RegisterItem.jute_seed, -100, pos, world);
			}
		}
		

		return super.removedByPlayer(world, pos, player, willHarvest);
	}
	
	@Override
	protected boolean canPlaceBlockOn(Block ground) {
		return true;
	}
	
	
}

