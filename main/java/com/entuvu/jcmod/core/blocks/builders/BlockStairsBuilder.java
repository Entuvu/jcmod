package com.entuvu.jcmod.core.blocks.builders;

import java.util.Random;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.event.DropSpecial;
import com.entuvu.jcmod.core.register.RegisterItem;

import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

public class BlockStairsBuilder extends BlockStairs {
	
	Item toDrop;
	int meta;
	int min;
	int random;
	
	public BlockStairsBuilder(IBlockState defaultState, String unlocalizedName, Item toDrop, int meta, int min, int random) {
		super(defaultState);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(JCMain.tabStandard);
		this.meta = meta;
		this.min = min;
		this.random = random;
		this.toDrop = toDrop;
	}

	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		return null;
	}
	
	@Override
	public boolean removedByPlayer(World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
		boolean didWork = super.removedByPlayer(world, pos, player, willHarvest);
		if (!world.isRemote && world.getGameRules().getGameRuleBooleanValue("doTileDrops") && didWork && !player.capabilities.isCreativeMode){
			DropSpecial.drop(this.min, this.random, this.toDrop, this.meta, pos, world);			
	    }
		return didWork;
	}
}
