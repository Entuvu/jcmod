package com.entuvu.jcmod.core.blocks.builders;

import com.entuvu.jcmod.JCMain;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockBasicBuilder extends Block {
	public BlockBasicBuilder(String unlocalizedName, Material material, float hardness, float resistance){
		super(material);
		this.setCreativeTab(JCMain.tabStandard);
		this.setUnlocalizedName(unlocalizedName);
		this.setHardness(hardness);
		this.setResistance(resistance);
	}
	
	public BlockBasicBuilder(String unlocalizedName, float hardness, float resistance){
		this(unlocalizedName, Material.rock, hardness, resistance);
	}
	
	public BlockBasicBuilder(String unlocalizedName){
		this(unlocalizedName, 2.0f, 2.0f);
	}
}
