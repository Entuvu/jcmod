package com.entuvu.jcmod.core.blocks.builders;

import com.entuvu.jcmod.core.event.DropSpecial;
import com.entuvu.jcmod.core.register.RegisterBlock;
import com.entuvu.jcmod.core.register.RegisterItem;

import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import scala.util.Random;

public class SpecialBlocks {
	
	public static final int COPPER_ORE = 0;
	public static final int TIN_ORE = 1;
	public static final int LEAD_ORE = 2;
	public static final int SILVER_ORE = 3;
	public static final int ANDESITE_COBBLESTONE = 4;
	public static final int DIORITE_COBBLESTONE = 5;
	public static final int GRANITE_COBBLESTONE = 6;
	
	public static void Drop(int blockID, World world, BlockPos pos, int fortune){
		switch(blockID){
		case COPPER_ORE:
			DropSpecial.drop(1 + GetForutne(fortune), 1, RegisterItem.copper_chunk, -100, pos, world);
			DropSpecial.drop(0 + GetForutne(fortune), 1, RegisterItem.rock, -100, pos, world);
			break;
		case TIN_ORE:
			DropSpecial.drop(1 + GetForutne(fortune), 1, RegisterItem.tin_chunk, -100, pos, world);
			DropSpecial.drop(0 + GetForutne(fortune), 1, RegisterItem.rock, -100, pos, world);
			break;
		case LEAD_ORE:
			DropSpecial.drop(1 + GetForutne(fortune), 1, RegisterItem.lead_chunk, -100, pos, world);
			DropSpecial.drop(0 + GetForutne(fortune), 1, RegisterItem.rock, -100, pos, world);
			break;
		case SILVER_ORE:
			DropSpecial.drop(1 + GetForutne(fortune), 1, RegisterItem.silver_chunk, -100, pos, world);
			DropSpecial.drop(0 + GetForutne(fortune), 1, RegisterItem.rock, -100, pos, world);
			break;
		case ANDESITE_COBBLESTONE:
			DropSpecial.drop(1, 0, Item.getItemFromBlock(RegisterBlock.cobblestone_andesite), -100, pos, world);
			break;
		case DIORITE_COBBLESTONE:
			DropSpecial.drop(1, 0, Item.getItemFromBlock(RegisterBlock.cobblestone_diorite), -100, pos, world);
			break;
		case GRANITE_COBBLESTONE:
			DropSpecial.drop(1, 0, Item.getItemFromBlock(RegisterBlock.cobblestone_granite), -100, pos, world);
			break;
		}
	}

	private static int GetForutne(int fortune) {
		Random rand = new Random();
		int rnd;
		if(fortune > 1){
			rnd = rand.nextInt(fortune);
		}else{
			rnd = 0;
		}
		return rnd;
	}

	public static Material getMaterial(int blockID){
		switch(blockID){
		case COPPER_ORE:
			return Material.rock;
		}
		return Material.rock;
	}
	
	public static float getHardness(int blockID){
		switch(blockID){
		case COPPER_ORE:
			return 2.8f;
		}
		return 1.0f;
	}
	
	public static String getTool(int blockID){
		switch(blockID){
		case COPPER_ORE: 
			return "pickaxe";
		}
		return "pickaxe";
	}

	public static int getToolLevel(int blockID) {
		switch(blockID){
		case COPPER_ORE: 
			return 0;
		}
		return 0;
	}
}
