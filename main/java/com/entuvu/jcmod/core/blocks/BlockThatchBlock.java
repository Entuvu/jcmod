package com.entuvu.jcmod.core.blocks;

import java.util.Random;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.register.RegisterBlock;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;

public class BlockThatchBlock extends Block {
	public BlockThatchBlock(String unlocalizedName) {
		super(Material.grass);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(JCMain.tabStandard);
		this.setHardness(0.6f);
		this.setHarvestLevel("axe", 0);
	}
}
