package com.entuvu.jcmod.core.blocks.typeEntity;

import java.util.Random;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.blocks.builders.BlockWithFacing;
import com.entuvu.jcmod.core.network.JCGuiHandler;
import com.entuvu.jcmod.core.tileentity.TileEntityAlloyFurnace;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockAlloyFurnace extends BlockWithFacing implements ITileEntityProvider {

	public BlockAlloyFurnace(String unlocalizedName) {
		super(Material.rock);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(JCMain.tabStandard);
		this.setHardness(2.0f);
		this.setResistance(25.0f);
		this.setHarvestLevel("pickaxe", 1);
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityAlloyFurnace();
	}
	
	@Override
	public int getRenderType() {
		return 3;
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		TileEntity tileentity = worldIn.getTileEntity(pos);

        if (tileentity instanceof TileEntityAlloyFurnace)
        {
            InventoryHelper.dropInventoryItems(worldIn, pos, (TileEntityAlloyFurnace)tileentity);
            worldIn.updateComparatorOutputLevel(pos, this);
        }
        
        super.breakBlock(worldIn, pos, state);
	}
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		if (stack.hasDisplayName())
        {
            TileEntity tileentity = worldIn.getTileEntity(pos);

            if (tileentity instanceof TileEntityAlloyFurnace)
            {
                ((TileEntityAlloyFurnace)tileentity).setCustomName(stack.getDisplayName());
            }
        }
		
		super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ) {
		
		if (worldIn.isRemote)
        {
            return true;
        }
        else
        {
        	TileEntity tileentity = worldIn.getTileEntity(pos);
            if (tileentity instanceof TileEntityAlloyFurnace)
            {
            	playerIn.openGui(JCMain.instance, JCGuiHandler.TILE_ENTITY_ALLOY_FURNACE_GUI, worldIn, pos.getX(), pos.getY(), pos.getZ());
            }
            return true;
		}
		
		
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World worldIn, BlockPos pos, IBlockState state, Random rand) {
		if(((TileEntityAlloyFurnace) worldIn.getTileEntity(pos)).isBurning()){
			EnumFacing enumfacing = (EnumFacing)state.getValue(FACING);
	        double d0 = (double)pos.getX() + 0.5D;
	        double d1 = (double)pos.getY() + rand.nextDouble() * 6.0D / 16.0D;
	        double d2 = (double)pos.getZ() + 0.5D;
	        double d3 = 0.52D;
	        double d4 = rand.nextDouble() * 0.6D - 0.3D;

	        switch (BlockAlloyFurnace.SwitchEnumFacing.FACING_LOOKUP[enumfacing.ordinal()])
	        {
	            case 1:
	                worldIn.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d0 - d3, d1, d2 + d4, 0.0D, 0.0D, 0.0D, new int[0]);
	                worldIn.spawnParticle(EnumParticleTypes.FLAME, d0 - d3, d1, d2 + d4, 0.0D, 0.0D, 0.0D, new int[0]);
	                break;
	            case 2:
	                worldIn.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d0 + d3, d1, d2 + d4, 0.0D, 0.0D, 0.0D, new int[0]);
	                worldIn.spawnParticle(EnumParticleTypes.FLAME, d0 + d3, d1, d2 + d4, 0.0D, 0.0D, 0.0D, new int[0]);
	                break;
	            case 3:
	                worldIn.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d0 + d4, d1, d2 - d3, 0.0D, 0.0D, 0.0D, new int[0]);
	                worldIn.spawnParticle(EnumParticleTypes.FLAME, d0 + d4, d1, d2 - d3, 0.0D, 0.0D, 0.0D, new int[0]);
	                break;
	            case 4:
	                worldIn.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d0 + d4, d1, d2 + d3, 0.0D, 0.0D, 0.0D, new int[0]);
	                worldIn.spawnParticle(EnumParticleTypes.FLAME, d0 + d4, d1, d2 + d3, 0.0D, 0.0D, 0.0D, new int[0]);
	        }
		}
	}
}