package com.entuvu.jcmod.core.blocks.typeEntity;

import java.util.Random;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.blocks.builders.BlockWithFacing;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;

public class BlockStonecuttersBench extends BlockWithFacing {
	public BlockStonecuttersBench(String unlocalizedName) {
		super(Material.rock);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(JCMain.tabStandard);
		this.setHardness(2.5f);
		this.setHarvestLevel("pickaxe", 1);
		this.setBlockBounds(0.0f, 0.0f, 0.0f, 1.0f, 0.95f, 1.0f);
	}
	
	@Override
	public int getRenderType() {
		return 3;
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
    public boolean isFullCube() {
        return false;
    }
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		return Item.getItemFromBlock(state.getBlock());
	}
}
