package com.entuvu.jcmod.core.blocks.typeEntity;

import java.util.Random;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.blocks.builders.BlockWithFacing;
import com.entuvu.jcmod.core.network.JCGuiHandler;
import com.entuvu.jcmod.core.tileentity.TileEntityCampFire;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockCampFire extends BlockWithFacing implements ITileEntityProvider{

	public BlockCampFire(String unlocalizedName) {
		super(Material.rock);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(JCMain.tabStandard);
		this.setHardness(2.0f);
		this.setResistance(25.0f);
		this.setHarvestLevel("pickaxe", 1);
		this.setBlockBounds(0.025f, 0f, 0.025f, 0.925f, 0.3f, 0.925f);
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityCampFire();
	}
	
	@Override
	public int getRenderType() {
		return 3;
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
    public boolean isFullCube() {
        return false;
    }

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {

        TileEntity tileentity = worldIn.getTileEntity(pos);

        if (tileentity instanceof TileEntityCampFire)
        {
            InventoryHelper.dropInventoryItems(worldIn, pos, (TileEntityCampFire)tileentity);
            worldIn.updateComparatorOutputLevel(pos, this);
        }

        super.breakBlock(worldIn, pos, state);
	}
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
		
		if (stack.hasDisplayName())
        {
            TileEntity tileentity = worldIn.getTileEntity(pos);

            if (tileentity instanceof TileEntityCampFire)
            {
                ((TileEntityCampFire)tileentity).setCustomName(stack.getDisplayName());
            }
        }
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (worldIn.isRemote)
        {
            return true;
        }
        else
        {
        	TileEntity tileentity = worldIn.getTileEntity(pos);

            if (tileentity instanceof TileEntityCampFire)
            {
            	playerIn.openGui(JCMain.instance, JCGuiHandler.TILE_ENTITY_CAMP_FIRE_GUI, worldIn, pos.getX(), pos.getY(), pos.getZ());
            }
            return true;
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World worldIn, BlockPos pos, IBlockState state, Random rand) {
		if(((TileEntityCampFire) worldIn.getTileEntity(pos)).isBurning()){
	        double d0 = (double)pos.getX() + .5d + rand.nextDouble() * .2d - rand.nextDouble() * .2d ;
	        double d1 = (double)pos.getY() + rand.nextDouble() * 6.0D / 16.0D;
	        double d2 = (double)pos.getZ() + .5d + rand.nextDouble() * .2d - rand.nextDouble() * .2d ;
	        double d4 = rand.nextDouble() * 0.6D - 0.3D;

	        
	        worldIn.spawnParticle(EnumParticleTypes.SMOKE_LARGE, d0, d1, d2 + d4, 0.0D, 0.0D, 0.0D, new int[0]);
	        worldIn.spawnParticle(EnumParticleTypes.FLAME, d0, d1, d2 + d4, 0.0D, 0.0D, 0.0D, new int[0]);

		}
	}
	
}
