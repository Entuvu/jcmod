package com.entuvu.jcmod.core.blocks.typeEntity;

import java.util.Random;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.blocks.builders.BlockWithFacing;
import com.entuvu.jcmod.core.event.DropSpecial;
import com.entuvu.jcmod.core.register.RegisterItem;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

public class BlockWoodStump extends BlockWithFacing {
	public BlockWoodStump(String unlocalizedName) {
		super(Material.wood);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(JCMain.tabStandard);
		this.setHardness(2.5f);
		this.setHarvestLevel("axe", 1);
		this.setBlockBounds(0.25f, 0.0f, 0.0625f, 0.75f, 0.8548f, 0.95f);
	}
	
	@Override
	public int getRenderType() {
		return 3;
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
    public boolean isFullCube() {
        return false;
    }
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		return null;
	}
	
	@Override
	public boolean removedByPlayer(World world, BlockPos pos, EntityPlayer player, boolean willHarvest) {
		boolean didWork = super.removedByPlayer(world, pos, player, willHarvest);
		if (!world.isRemote && world.getGameRules().getGameRuleBooleanValue("doTileDrops") && didWork && !player.capabilities.isCreativeMode){
			DropSpecial.drop(1, 2, RegisterItem.plank_oak, -100, pos, world);			
	    }
		return didWork;
	}
}
