package com.entuvu.jcmod.core.testing;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.blocks.builders.BlockWithFacing;
import com.entuvu.jcmod.core.network.JCGuiHandler;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class BlockTestMixer extends BlockWithFacing implements ITileEntityProvider {

	public BlockTestMixer(String unlocalizedName) {
		super(Material.wood);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(JCMain.tabStandard);
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	public boolean isFullBlock() {
		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityTestMixer();
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		TileEntity tileentity = worldIn.getTileEntity(pos);

        if (tileentity instanceof TileEntityTestMixer)
        {
            InventoryHelper.dropInventoryItems(worldIn, pos, (TileEntityTestMixer)tileentity);
            worldIn.updateComparatorOutputLevel(pos, this);
        }

        super.breakBlock(worldIn, pos, state);
	}
	
	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer,
			ItemStack stack) {
		if (stack.hasDisplayName())
        {
            TileEntity tileentity = worldIn.getTileEntity(pos);

            if (tileentity instanceof TileEntityTestMixer)
            {
                ((TileEntityTestMixer)tileentity).setCustomName(stack.getDisplayName());
            }
        }
		super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn,
			EnumFacing side, float hitX, float hitY, float hitZ) {
		if (worldIn.isRemote)
        {
            return true;
        }
        else
        {
        	TileEntity tileentity = worldIn.getTileEntity(pos);
            if (tileentity instanceof TileEntityTestMixer)
            {
            	playerIn.openGui(JCMain.instance, JCGuiHandler.TILE_ENTITY_TEST_MIXER, worldIn, pos.getX(), pos.getY(), pos.getZ());
            }
            return true;
		}
	}	

}
