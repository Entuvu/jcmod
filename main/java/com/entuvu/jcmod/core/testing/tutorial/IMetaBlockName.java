package com.entuvu.jcmod.core.testing.tutorial;

import net.minecraft.item.ItemStack;

public interface IMetaBlockName {
        String getSpecialName(ItemStack stack);
}