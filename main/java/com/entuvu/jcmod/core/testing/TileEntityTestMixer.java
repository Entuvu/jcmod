package com.entuvu.jcmod.core.testing;

import java.util.ArrayList;

import com.entuvu.jcmod.core.crafting.MixingTableTest;
import com.entuvu.jcmod.core.crafting.ShapelessRecipesWithContainer;
import com.entuvu.jcmod.core.register.RegisterItem;
import com.google.common.collect.Lists;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.IChatComponent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityTestMixer extends TileEntity implements IInventory{

	private String customName;
	private ItemStack[] mixingBoxInventory;
	private MixingTableTest mt; 
	
	public TileEntityTestMixer(){
		this.mixingBoxInventory = new ItemStack[this.getSizeInventory()];
		 mt = MixingTableTest.getInstance();
	}

	@Override
	public String getName() {
		return this.hasCustomName() ? this.customName : "container.jcmod_test_mixer";
	}

	@Override
	public boolean hasCustomName() {
		return this.customName != null && !this.customName.equals("");
	}

	@Override
	public IChatComponent getDisplayName() {
		return this.hasCustomName() ? new ChatComponentText(this.getName()) : new ChatComponentTranslation(this.getName());
	}

	@Override
	public int getSizeInventory() {
		return 6;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		if(index < 0 || index >= getSizeInventory())
			return null;
		return this.mixingBoxInventory[index];
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		if(this.getStackInSlot(index) != null ){
			ItemStack itemstack;
			if(this.getStackInSlot(index).stackSize <= count){
				itemstack = this.getStackInSlot(index);
				this.setInventorySlotContents(index, null);
				this.markDirty();
				return itemstack;
			}else{
				itemstack = this.getStackInSlot(index).splitStack(count);
				if(this.getStackInSlot(index).stackSize<=0){
					this.setInventorySlotContents(index, null);
				}else{
					this.setInventorySlotContents(index, this.getStackInSlot(index));
				}
			}
			
			this.markDirty();
			return itemstack;
		}else{
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int index) {
		ItemStack stack = this.getStackInSlot(index);
		this.setInventorySlotContents(index, null);
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		boolean flag = stack != null && stack.isItemEqual(this.mixingBoxInventory[index]) && ItemStack.areItemsEqual(stack, this.mixingBoxInventory[index]);
		this.mixingBoxInventory[index] = stack;
		
		//System.out.println(String.valueOf(index) + " :: " + stack + " :: " + String.valueOf(flag));
		
		if(stack != null && stack.stackSize > this.getInventoryStackLimit()){
			stack.stackSize = this.getInventoryStackLimit();
		}
		
		ShowOutput();
		
		if(!flag){
			this.markDirty();
		}
	}

	@SideOnly(Side.SERVER)
	private void ShowOutput() {
		System.out.println("Showing output");
		if(this.mixingBoxInventory[0] != null && this.mixingBoxInventory[1] != null && this.mixingBoxInventory[2] != null && this.mixingBoxInventory[3]!=null && this.mixingBoxInventory[4] != null){
			if(isItemContainer(this.mixingBoxInventory[0])){
				ArrayList arraylist = Lists.newArrayList();
				arraylist.add(this.mixingBoxInventory[1]);
				arraylist.add(this.mixingBoxInventory[2]);
				arraylist.add(this.mixingBoxInventory[3]);
				arraylist.add(this.mixingBoxInventory[4]);
				ShapelessRecipesWithContainer currentRecipe = new ShapelessRecipesWithContainer(mixingBoxInventory[0].getItem(), arraylist);
				
				for(int x = 0; x < mt.recipes.size(); x++){
					System.out.println(((ShapelessRecipesWithContainer)mt.recipes.get(x)).recipeItems);
					System.out.println(currentRecipe.recipeItems);
					if(HasMatch((ShapelessRecipesWithContainer)mt.recipes.get(x), currentRecipe)){
						System.out.println("HAS A MATCH");
					}else{
						System.out.println("No Match");
					}
				}
				
			}
		}
	}

	private boolean HasMatch(ShapelessRecipesWithContainer currentRecipe, ShapelessRecipesWithContainer entry) {
		if(currentRecipe.recipeContainer == entry.recipeContainer){
			System.out.println(currentRecipe.recipeItems.get(3).getUnlocalizedName());
			System.out.println(entry.recipeItems.get(3).getUnlocalizedName());
			if(currentRecipe.recipeItems.get(0).getUnlocalizedName() == entry.recipeItems.get(0).getUnlocalizedName()){
				System.out.println("01 no match");
				if(currentRecipe.recipeItems.get(1).getUnlocalizedName() == entry.recipeItems.get(1).getUnlocalizedName()){
					if(currentRecipe.recipeItems.get(2).getUnlocalizedName() == entry.recipeItems.get(2).getUnlocalizedName()){
						if(currentRecipe.recipeItems.get(3).getUnlocalizedName() == entry.recipeItems.get(3).getUnlocalizedName()){
							
							return true;
						}
					}
				}
				
			}
		}
		return false;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.getPos()) == this && player.getDistanceSq(this.pos.add(0.5,0.5,0.5)) <= 64;
	}

	@Override
	public void openInventory(EntityPlayer player) {
	}

	@Override
	public void closeInventory(EntityPlayer player) {	
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		if(index == 0 && isItemContainer(stack)) return true;
		return false;
	}

	@Override
	public int getField(int id) {
		return 0;
	}

	@Override
	public void setField(int id, int value) {
	}

	@Override
	public int getFieldCount() {
		return 0;
	}

	@Override
	public void clear() {
		for(int i = 0; i < this.getSizeInventory(); i ++)
			this.setInventorySlotContents(i, null);
	}

	public void setCustomName(String displayName) {
		this.customName = displayName;
	}

	public static boolean isItemContainer(ItemStack stack) {
		if(stack.getItem() == RegisterItem.burlap_sack) return true;
		return false;
	}
	
	
	
}
