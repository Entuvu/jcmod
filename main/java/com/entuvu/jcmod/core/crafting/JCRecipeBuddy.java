package com.entuvu.jcmod.core.crafting;

import net.minecraft.item.ItemStack;

public class JCRecipeBuddy {

	public ItemStack input1;
	public ItemStack input2;
	
	public JCRecipeBuddy(ItemStack input, ItemStack utensil) {
		this.input2 = input;
		this.input1 = utensil;
	}
	
}
