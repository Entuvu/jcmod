package com.entuvu.jcmod.core.crafting;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.entuvu.jcmod.core.register.RegisterItem;
import com.google.common.collect.Maps;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class SmeltingFurnaceRecipes {
	private static final SmeltingFurnaceRecipes sfBase = new SmeltingFurnaceRecipes();
	private Map smeltingList = Maps.newHashMap();
	private Map experienceList = Maps.newHashMap();
	
	public static SmeltingFurnaceRecipes instance(){
		return sfBase;
	}
	
	private SmeltingFurnaceRecipes(){
		this.addSmeltingFurnaceRecipe(RegisterItem.ingot_mold, RegisterItem.copper_chunk, new ItemStack(RegisterItem.copper_ingot), 100f); //Need 2 chunks		
		this.addSmeltingFurnaceRecipe(RegisterItem.ingot_mold, RegisterItem.gold_chunk, new ItemStack(Items.gold_ingot), 100f); //Need 2 chunks		
		this.addSmeltingFurnaceRecipe(RegisterItem.ingot_mold, RegisterItem.iron_chunk, new ItemStack(Items.iron_ingot), 100f); //Need 2 chunks		
		this.addSmeltingFurnaceRecipe(RegisterItem.ingot_mold, RegisterItem.tin_chunk, new ItemStack(RegisterItem.tin_ingot), 100f); //Need 2 chunks		
		this.addSmeltingFurnaceRecipe(RegisterItem.ingot_mold, RegisterItem.lead_chunk, new ItemStack(RegisterItem.lead_ingot), 100f); //Need 2 chunks		
		this.addSmeltingFurnaceRecipe(RegisterItem.ingot_mold, RegisterItem.silver_chunk, new ItemStack(RegisterItem.silver_ingot), 100f); //Need 2 chunks			
		this.addSmeltingFurnaceRecipe(RegisterItem.bar_mold, RegisterItem.copper_nugget, new ItemStack(RegisterItem.copper_bar), 100f); //Need 2 nuggets		
		this.addSmeltingFurnaceRecipe(RegisterItem.bar_mold, RegisterItem.gold_nugget, new ItemStack(RegisterItem.gold_bar), 100f); //Need 2 nuggets		
		this.addSmeltingFurnaceRecipe(RegisterItem.bar_mold, RegisterItem.iron_nugget, new ItemStack(RegisterItem.iron_bar), 100f); //Need 2 nuggets		
		this.addSmeltingFurnaceRecipe(RegisterItem.bar_mold, RegisterItem.tin_nugget, new ItemStack(RegisterItem.tin_bar), 100f); //Need 2 nuggets
		this.addSmeltingFurnaceRecipe(RegisterItem.nail_mold, RegisterItem.iron_nugget, new ItemStack(RegisterItem.iron_nails, 4), 100f); //Need 2 nuggets
		this.addSmeltingFurnaceRecipe(RegisterItem.arrowhead_mold, RegisterItem.copper_nugget, new ItemStack(RegisterItem.copper_arrowhead, 2), 100f); //Need 2 nuggets
		this.addSmeltingFurnaceRecipe(RegisterItem.hinge_mold, RegisterItem.iron_nugget, new ItemStack(RegisterItem.iron_hinge, 2), 100f); //Need 2 nuggets
	}
	
	/**
     * Adds a cooking recipe using an Item as the input item.
     *  
     * @param mold The input Item to be used for this recipe.
     * @param metal The utensil Item to be used for this recipe.
     * @param outputStack The output ItemStack for this recipe.
     * @param experience The amount of experience this recipe will give the player.
     */
	public void addSmelting(Item mold, Item metal, ItemStack outputStack, float experience){
		this.addSmeltingFurnaceRecipe(mold, metal, outputStack, experience);
	}
	
	
	/**
	 * Adds a cooking recipe using an ItemStack as the input for the recipe.
	 * 
	 * @param mold The input ItemStack for this recipe
	 * @param metal The utensil used to cook this recipe.
	 * @param outputStack The output ItemStack for this recipe.
	 * @param experience The amount of experience this recipe will give the player.
	 */
	public void addSmeltingFurnaceRecipe(Item mold, Item metal, ItemStack outputStack, float experience){
		this.smeltingList.put(new JCRecipeBuddy(new ItemStack(mold, 1, 32767), new ItemStack(metal, 1, 32767)), outputStack);
		this.experienceList.put(new JCRecipeBuddy(new ItemStack(mold, 1, 32767), new ItemStack(metal, 1, 32767)), Float.valueOf(experience));
	}
	
	/*
	 * Returns the cooking result of items.
	 */
	public ItemStack getSmeltingResult(JCRecipeBuddy sfRecipe){
		Iterator iterator = this.smeltingList.entrySet().iterator();
		Entry entry;
		
		do{
			if(!iterator.hasNext()){
				return null;
			}
			
			entry = (Entry)iterator.next();
		}
		while(!this.compareItemStacks(sfRecipe, (JCRecipeBuddy)entry.getKey()));
		
		return (ItemStack)entry.getValue();
	}

	private boolean compareItemStacks(JCRecipeBuddy stack1, JCRecipeBuddy stack2) {
		ItemStack stack1Mold;
		ItemStack stack2Mold;
		ItemStack stack1Metal;
		ItemStack stack2Metal;
		
		stack1Mold = stack1.input2;
		stack2Mold = stack2.input2;
		stack1Metal = stack1.input1;
		stack2Metal = stack2.input1;
		
		if(stack2Metal == null)
			stack2Metal = new ItemStack(Blocks.air, 1,32767 );
		
		if(stack2Mold == null)
			stack2Mold = new ItemStack(Blocks.air, 1,32767 );
		
		if(stack1Mold == null)
			stack1Mold = new ItemStack(Blocks.air, 1,32767 );
		
		if(stack1Metal == null)
			stack1Metal = new ItemStack(Blocks.air, 1,32767 );
		
		return (stack2Mold.getItem() == stack1Mold.getItem() && 
				(stack2Mold.getMetadata() == 32767 || 
						stack2Mold.getMetadata() == stack1Mold.getMetadata())) && 
				(stack2Metal.getItem() == stack1Metal.getItem() && 
				(stack2Metal.getMetadata() == 32767 || 
						stack2Metal.getMetadata() == stack1Metal.getMetadata()));
	}
	
	public Map getSmeltingList(){
		return this.smeltingList;
	}
	
	public float getSmeltingExperience(JCRecipeBuddy stack){
		Iterator iterator = this.experienceList.entrySet().iterator();
        Entry entry;

        do
        {
            if (!iterator.hasNext())
            {
                return 0.0F;
            }

            entry = (Entry)iterator.next();
        }
        while (!this.compareItemStacks(stack, (JCRecipeBuddy)entry.getKey()));

        return ((Float)entry.getValue()).floatValue();
	}
	}
