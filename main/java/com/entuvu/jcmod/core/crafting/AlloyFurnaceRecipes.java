package com.entuvu.jcmod.core.crafting;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.entuvu.jcmod.core.register.RegisterItem;
import com.google.common.collect.Maps;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class AlloyFurnaceRecipes {
	private static final AlloyFurnaceRecipes afBase = new AlloyFurnaceRecipes();
	/** The list of cooking results **/
	private Map smeltingList = Maps.newHashMap();
	/** A list that contains experience for each recipe **/
	private Map experienceList = Maps.newHashMap();
	
	public static AlloyFurnaceRecipes instance(){
		return afBase;
	}
	
	private AlloyFurnaceRecipes(){
		this.addAlloySmeltingRecipe(RegisterItem.copper_ingot, RegisterItem.tin_ingot, new ItemStack(RegisterItem.bronze_ingot, 2), 100f);
		this.addAlloySmeltingRecipe(Items.iron_ingot, Items.coal, new ItemStack(RegisterItem.steel_ingot, 2), 100f);
	}
	
	/**
     * Adds a cooking recipe using an Item as the input item.
     *  
     * @param alloy1 The input Item to be used for this recipe.
     * @param alloy2 The utensil Item to be used for this recipe.
     * @param outputStack The output ItemStack for this recipe.
     * @param experience The amount of experience this recipe will give the player.
     */
	public void addAlloySmelting(Item alloy1, Item alloy2, ItemStack outputStack, float experience){
		this.addAlloySmeltingRecipe(alloy1, alloy2, outputStack, experience);
	}
	
	
	/**
	 * Adds a cooking recipe using an ItemStack as the input for the recipe.
	 * 
	 * @param alloy1 The input ItemStack for this recipe
	 * @param alloy2 The utensil used to cook this recipe.
	 * @param outputStack The output ItemStack for this recipe.
	 * @param experience The amount of experience this recipe will give the player.
	 */
	public void addAlloySmeltingRecipe(Item alloy1, Item alloy2, ItemStack outputStack, float experience){
		this.smeltingList.put(new JCRecipeBuddy(new ItemStack(alloy1, 1, 32767), new ItemStack(alloy2, 1, 32767)), outputStack);
		this.experienceList.put(new JCRecipeBuddy(new ItemStack(alloy1, 1, 32767), new ItemStack(alloy2, 1, 32767)), Float.valueOf(experience));
		
		this.smeltingList.put(new JCRecipeBuddy(new ItemStack(alloy2, 1, 32767), new ItemStack(alloy1, 1, 32767)), outputStack);
		this.experienceList.put(new JCRecipeBuddy(new ItemStack(alloy2, 1, 32767), new ItemStack(alloy1, 1, 32767)), Float.valueOf(experience));
	}
	
	/*
	 * Returns the cooking result of items.
	 */
	public ItemStack getSmeltingResult(JCRecipeBuddy afRecipe){
		
		Iterator iterator = this.smeltingList.entrySet().iterator();
		Entry entry;
		
		do{
			if(!iterator.hasNext()){
				return null;
			}
			
			entry = (Entry)iterator.next();
		}
		while(!this.compareItemStacks(afRecipe, (JCRecipeBuddy)entry.getKey()));
		
		return (ItemStack)entry.getValue();
	}

	private boolean compareItemStacks(JCRecipeBuddy stack1, JCRecipeBuddy stack2) {
		ItemStack stack1Alloy1;
		ItemStack stack2Alloy1;
		ItemStack stack1Alloy2;
		ItemStack stack2Alloy2;
		
		stack1Alloy1 = stack1.input2;
		stack2Alloy1 = stack2.input2;
		stack1Alloy2 = stack1.input1;
		stack2Alloy2 = stack2.input1;
		
		if(stack1Alloy2 == null)
			stack1Alloy2 = new ItemStack(Blocks.air, 1,32767 );
		
		return (stack2Alloy1.getItem() == stack1Alloy1.getItem() && 
				(stack2Alloy1.getMetadata() == 32767 || 
						stack2Alloy1.getMetadata() == stack1Alloy1.getMetadata())) && 
				(stack2Alloy2.getItem() == stack1Alloy2.getItem() && 
				(stack2Alloy2.getMetadata() == 32767 || 
						stack2Alloy2.getMetadata() == stack1Alloy2.getMetadata()));
	}
	
	public Map getSmeltingList(){
		return this.smeltingList;
	}
	
	public float getSmeltingExperience(JCRecipeBuddy stack){
		Iterator iterator = this.experienceList.entrySet().iterator();
        Entry entry;

        do
        {
            if (!iterator.hasNext())
            {
                return 0.0F;
            }

            entry = (Entry)iterator.next();
        }
        while (!this.compareItemStacks(stack, (JCRecipeBuddy)entry.getKey()));

        return ((Float)entry.getValue()).floatValue();
	}
}
