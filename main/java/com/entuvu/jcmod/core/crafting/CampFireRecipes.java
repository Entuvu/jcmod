package com.entuvu.jcmod.core.crafting;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.entuvu.jcmod.core.register.RegisterItem;
import com.google.common.collect.Maps;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFishFood;
import net.minecraft.item.ItemStack;

public class CampFireRecipes {
	private static final CampFireRecipes cfBase = new CampFireRecipes();
	/** The list of cooking results **/
	private Map cookingList = Maps.newHashMap();
	/** A list that contains experience for each recipe **/
	private Map experienceList = Maps.newHashMap();
	
	public static CampFireRecipes instance(){
		return cfBase;
	}
	
	private CampFireRecipes(){
		this.addCookingRecipe(Items.chicken, Items.stick, new ItemStack(Items.cooked_chicken, 1), 100f);
		this.addCookingRecipe(Items.beef, Items.stick, new ItemStack(Items.cooked_beef, 1), 200f);
		this.addCookingRecipe(Items.mutton, Items.stick, new ItemStack(Items.cooked_mutton, 1), 200f);
		this.addCookingRecipe(Items.rabbit, Items.stick, new ItemStack(Items.cooked_rabbit, 1), 100f);
		this.addCookingRecipe(Items.porkchop, Items.stick, new ItemStack(Items.cooked_porkchop, 1), 200f);
		this.addCookingRecipe(Items.egg, RegisterItem.tin_can, new ItemStack(RegisterItem.boiled_egg, 1), 50f);
		
		
        ItemFishFood.FishType[] afishtype = ItemFishFood.FishType.values();
        int i = afishtype.length;

        for (int j = 0; j < i; ++j)
        {
            ItemFishFood.FishType fishtype = afishtype[j];

            if (fishtype.canCook())
            {
                this.addCookingRecipe(new ItemStack(Items.fish, 1, fishtype.getMetadata()), Items.stick,  new ItemStack(Items.cooked_fish, 1, fishtype.getMetadata()), 0.35F);
            }
        }
	}
	
	
	
	private void addCookingRecipe(ItemStack rawStack, Item utensil, ItemStack stack, float experience) {
		this.cookingList.put(new JCRecipeBuddy(rawStack, new ItemStack(utensil, 1)), stack);
		this.experienceList.put(new JCRecipeBuddy(rawStack, new ItemStack(utensil, 1)), Float.valueOf(experience));
	}

	/**
	 * Adds a cooking recipe where the input item is an instance of Block.
	 * 
	 * @param input The block to be used as the input for cooking recipe.
	 * @param utensil The utensil as item to be used for cooking recipe.
	 * @param stack The output for this recipe in the form of an ItemStack.
	 * @Param experience The amount of experience this recipe will give the player.
	 */
	public void addCookingRecipeForBlock(Block input, Item utensil, ItemStack stack, float experience){
		this.addCooking(Item.getItemFromBlock(input), utensil, stack, experience);
	}
	
	/**
     * Adds a cooking recipe using an Item as the input item.
     *  
     * @param input The input Item to be used for this recipe.
     * @param utensil The utensil Item to be used for this recipe.
     * @param stack The output ItemStack for this recipe.
     * @param experience The amount of experience this recipe will give the player.
     */
	public void addCooking(Item input, Item utensil, ItemStack stack, float experience){
		this.addCookingRecipe(input, utensil, stack, experience);
	}
	
	
	/**
	 * Adds a cooking recipe using an ItemStack as the input for the recipe.
	 * 
	 * @param food The input ItemStack for this recipe
	 * @param utensil The utensil used to cook this recipe.
	 * @param stack The output ItemStack for this recipe.
	 * @param experience The amount of experience this recipe will give the player.
	 */
	public void addCookingRecipe(Item food, Item utensil, ItemStack stack, float experience){
		this.cookingList.put(new JCRecipeBuddy(new ItemStack(food, 1), new ItemStack(utensil, 1)), stack);
		this.experienceList.put(new JCRecipeBuddy(new ItemStack(food, 1), new ItemStack(utensil, 1)), Float.valueOf(experience));
	}
	
	/*
	 * Returns the cooking result of items.
	 */
	public ItemStack getCookingResult(JCRecipeBuddy cfRecipe){
		Iterator iterator = this.cookingList.entrySet().iterator();
		Entry entry;
		
		do{
			if(!iterator.hasNext()){
				return null;
			}
			
			entry = (Entry)iterator.next();
		}
		while(!this.compareItemStacks(cfRecipe, (JCRecipeBuddy)entry.getKey()));
		
		return (ItemStack)entry.getValue();
	}

	private boolean compareItemStacks(JCRecipeBuddy stack1, JCRecipeBuddy stack2) {
		ItemStack stack1Input;
		ItemStack stack2Input;
		ItemStack stack1Utensil;
		ItemStack stack2Utensil;
		
		stack1Input = stack1.input2;
		stack2Input = stack2.input2;
		stack1Utensil = stack1.input1;
		stack2Utensil = stack2.input1;
		
		if(stack1Utensil == null)
			stack1Utensil = new ItemStack(Blocks.air, 1,32767 );
		
		return (stack2Input.getItem() == stack1Input.getItem() && 
				(stack2Input.getMetadata() == 32767 || 
						stack2Input.getMetadata() == stack1Input.getMetadata())) && 
				(stack2Utensil.getItem() == stack1Utensil.getItem() && 
				(stack2Utensil.getMetadata() == 32767 || 
						stack2Utensil.getMetadata() == stack1Utensil.getMetadata()));
	}
	
	public Map getCookingList(){
		return this.cookingList;
	}
	
	public float getCookingExperience(JCRecipeBuddy stack){
		Iterator iterator = this.experienceList.entrySet().iterator();
        Entry entry;

        do
        {
            if (!iterator.hasNext())
            {
                return 0.0F;
            }

            entry = (Entry)iterator.next();
        }
        while (!this.compareItemStacks(stack, (JCRecipeBuddy)entry.getKey()));

        return ((Float)entry.getValue()).floatValue();
	}

}


