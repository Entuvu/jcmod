package com.entuvu.jcmod.core.crafting;

import java.util.Comparator;
import java.util.List;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ShapelessRecipesWithContainer {

	public ItemStack recipeOutput;
	public Item recipeContainer;
	public List<ItemStack> recipeItems;

	public ShapelessRecipesWithContainer(ItemStack output, Item container, List<ItemStack> inputList) {
		this.recipeOutput = output;
		this.recipeContainer = container;
		this.recipeItems = inputList;
		SortRecipeItems();
	}
	
	public ShapelessRecipesWithContainer( Item container, List<ItemStack> inputList) {
		this.recipeOutput = null;
		this.recipeContainer = container;
		this.recipeItems = inputList;
		SortRecipeItems();
	}
	
	private void SortRecipeItems() {
		RecipeComparator test = new RecipeComparator();
		this.recipeItems.sort(test);
	}

	class RecipeComparator implements Comparator<ItemStack>{

		@Override
		public int compare(ItemStack arg0, ItemStack arg1) {
			return arg0.getUnlocalizedName().compareToIgnoreCase(arg1.getUnlocalizedName());
		}
	}

}
