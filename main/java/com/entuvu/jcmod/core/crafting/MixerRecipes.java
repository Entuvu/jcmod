package com.entuvu.jcmod.core.crafting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.entuvu.jcmod.core.register.RegisterBlock;
import com.entuvu.jcmod.core.register.RegisterItem;
import com.google.common.collect.Lists;

import net.minecraft.block.Block;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;

public class MixerRecipes {
	
	private static final MixerRecipes instance = new MixerRecipes();
	private final List recipes = Lists.newArrayList();
	
	public static MixerRecipes getInstance()
    {
        /** The static instance of this class */
        return instance;
    }
	
	private MixerRecipes(){
		this.addShapelessRecipe(
				new ItemStack(RegisterItem.bucket_mortar, 1), //Bucket of Mortar//
				new ItemStack(RegisterItem.pile_dirt), 
				new ItemStack(RegisterItem.pile_dirt),
				new ItemStack(Items.clay_ball), 
				new ItemStack(Items.clay_ball));
		
		this.addShapelessRecipe(
				new ItemStack(RegisterItem.bucket_mortar, 1), //Bucket of Mortar//
				new ItemStack(RegisterItem.pile_sand), 
				new ItemStack(RegisterItem.pile_sand),
				new ItemStack(Items.clay_ball), 
				new ItemStack(Items.clay_ball));
		
		this.addShapelessRecipe(
				new ItemStack(RegisterItem.bucket_compost, 1), //Bucket of Compost//
				new ItemStack(RegisterItem.pile_dirt), 
				new ItemStack(RegisterItem.thatch),
				new ItemStack(Items.rotten_flesh), 
				new ItemStack(Items.fish));
		
		this.addShapelessRecipe(
				new ItemStack(RegisterItem.sack_fertilizer, 1), //Sack of Fertilizer//
				new ItemStack(RegisterItem.bucket_compost), 
				new ItemStack(RegisterItem.bucket_compost),
				new ItemStack(Items.dye, 1, 15),
				new ItemStack(Items.dye, 1, 15));
		
		Collections.sort(this.recipes, new Comparator()
        {
            public int compare(IRecipe p_compare_1_, IRecipe p_compare_2_)
            {
                return p_compare_1_ instanceof ShapelessRecipes && p_compare_2_ instanceof ShapedRecipes ? 1 : (p_compare_2_ instanceof ShapelessRecipes && p_compare_1_ instanceof ShapedRecipes ? -1 : (p_compare_2_.getRecipeSize() < p_compare_1_.getRecipeSize() ? -1 : (p_compare_2_.getRecipeSize() > p_compare_1_.getRecipeSize() ? 1 : 0)));
            }
            public int compare(Object p_compare_1_, Object p_compare_2_)
            {
                return this.compare((IRecipe)p_compare_1_, (IRecipe)p_compare_2_);
            }
        });
	}

	/**
     * Adds a shapeless crafting recipe to the the game.
     *  
     * @param recipeComponents An array of ItemStack's Item's and Block's that make up the recipe.
     */
    public void addShapelessRecipe(ItemStack stack, Object ... recipeComponents)
    {
        ArrayList arraylist = Lists.newArrayList();
        Object[] aobject = recipeComponents;
        int i = recipeComponents.length;

        for (int j = 0; j < i; ++j)
        {
            Object object1 = aobject[j];

            if (object1 instanceof ItemStack)
            {
                arraylist.add(((ItemStack)object1).copy());
            }
            else if (object1 instanceof Item)
            {
                arraylist.add(new ItemStack((Item)object1));
            }
            else
            {
                if (!(object1 instanceof Block))
                {
                    throw new IllegalArgumentException("Invalid shapeless recipe: unknown type " + object1.getClass().getName() + "!");
                }

                arraylist.add(new ItemStack((Block)object1));
            }
        }

        this.recipes.add(new ShapelessRecipes(stack, arraylist));
    }
    
    
    public void addRecipe(IRecipe recipe){
    	this.recipes.add(recipe);
    }
    
    public ItemStack findMatchingRecipe(InventoryCrafting itemMatrix, World worldIn)
    {
        Iterator iterator = this.recipes.iterator();
        IRecipe irecipe;

        do
        {
            if (!iterator.hasNext())
            {
                return null;
            }

            irecipe = (IRecipe)iterator.next();
        }
        while (!irecipe.matches(itemMatrix, worldIn));
        
        

        return irecipe.getCraftingResult(itemMatrix);
    }

    public ItemStack[] func_180303_b(InventoryCrafting p_180303_1_, World worldIn)
    {
        Iterator iterator = this.recipes.iterator();

        while (iterator.hasNext())
        {
            IRecipe irecipe = (IRecipe)iterator.next();

            if (irecipe.matches(p_180303_1_, worldIn))
            {
                return irecipe.getRemainingItems(p_180303_1_);
            }
        }

        ItemStack[] aitemstack = new ItemStack[p_180303_1_.getSizeInventory()];

        for (int i = 0; i < aitemstack.length; ++i)
        {
            aitemstack[i] = p_180303_1_.getStackInSlot(i);
        }

        return aitemstack;
    }

    /**
     * returns the List<> of all recipes
     */
    public List getRecipeList()
    {
        return this.recipes;
    }
	
}
