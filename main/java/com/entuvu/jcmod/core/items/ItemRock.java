package com.entuvu.jcmod.core.items;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.entity.projectile.EntityRock;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemSnowball;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemRock extends ItemSnowball{
	public ItemRock(String unlocalizedName){
		this.setUnlocalizedName(unlocalizedName);
		this.setMaxStackSize(64);
		this.setCreativeTab(JCMain.tabStandard);
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn) {
		if (!playerIn.capabilities.isCreativeMode)
        {
            --itemStackIn.stackSize;
        }

        //worldIn.playSoundAtEntity(playerIn, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

        if (!worldIn.isRemote)
        {
            worldIn.spawnEntityInWorld(new EntityRock(worldIn, playerIn));
        }

        return itemStackIn;
	}
}
