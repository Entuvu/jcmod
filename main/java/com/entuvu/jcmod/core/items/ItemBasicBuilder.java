package com.entuvu.jcmod.core.items;

import com.entuvu.jcmod.JCMain;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemBasicBuilder extends Item{
	public ItemBasicBuilder(String unlocalizedName){
		super();
		this.setCreativeTab(JCMain.tabStandard);
		this.setUnlocalizedName(unlocalizedName);
	}
	
	public ItemBasicBuilder(String unlocalizedName, int maxDamage){
		this(unlocalizedName);
		this.setHasSubtypes(true);
		this.isDamageable();
		this.setMaxDamage(maxDamage);
	}
	
	/* This is key for crafting damage - and vanilla crafting damage only so far. */
	@Override
	public ItemStack getContainerItem(ItemStack itemStack) {
		itemStack.setItemDamage(itemStack.getItemDamage()+1);
		return itemStack;
	}
	
	
}
