package com.entuvu.jcmod.core.items;

import com.entuvu.jcmod.JCMain;

import net.minecraft.item.ItemHoe;

public class ItemHoeBuilder extends ItemHoe {
	public ItemHoeBuilder(String unlocalizedName, ToolMaterial material) {
		super(material);
		this.setCreativeTab(JCMain.tabStandard);
		this.setUnlocalizedName(unlocalizedName);
	}
}
