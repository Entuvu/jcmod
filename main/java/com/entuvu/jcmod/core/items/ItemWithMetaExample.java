package com.entuvu.jcmod.core.items;

import java.util.List;

import com.entuvu.jcmod.JCMain;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemWithMetaExample extends Item{
	public ItemWithMetaExample(String unlocalizedName){
		super();
		this.setCreativeTab(JCMain.tabStandard);
		this.setHasSubtypes(true);
		this.setUnlocalizedName(unlocalizedName);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack) {
		return super.getUnlocalizedName() + "." + (stack.getItemDamage() == 0 ? "white" : "black");
	}
	
	@Override
	public void getSubItems(Item itemIn, CreativeTabs tab, List subItems) {
		subItems.add(new ItemStack(itemIn, 1, 0));
		subItems.add(new ItemStack(itemIn, 1, 1));
	}
}
