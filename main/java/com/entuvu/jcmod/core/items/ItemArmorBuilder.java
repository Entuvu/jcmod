package com.entuvu.jcmod.core.items;

import com.entuvu.jcmod.JCMain;

import net.minecraft.item.ItemArmor;

public class ItemArmorBuilder extends ItemArmor{
	public ItemArmorBuilder(String unlocalizedName, ArmorMaterial material, int renderIndex, int armorType){
		super(material, renderIndex, armorType);
		this.setCreativeTab(JCMain.tabStandard);
		this.setUnlocalizedName(unlocalizedName);
	}
}
