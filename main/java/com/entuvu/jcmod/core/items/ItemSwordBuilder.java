package com.entuvu.jcmod.core.items;

import com.entuvu.jcmod.JCMain;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;

public class ItemSwordBuilder extends ItemSword{

	public ItemSwordBuilder(String unlocalizedName, ToolMaterial material) {
		super(material);
		this.setCreativeTab(JCMain.tabStandard);
		this.setUnlocalizedName(unlocalizedName);
	}
	
	public ItemSwordBuilder(String unlocalizedName, ToolMaterial material, int maxDamage){
		this(unlocalizedName, material);
		this.setHasSubtypes(true);
		this.isDamageable();
		this.setMaxDamage(maxDamage);
	}
	
	/* This is key for crafting damage - and vanilla crafting damage only so far. */
	@Override
	public ItemStack getContainerItem(ItemStack itemStack) {
		itemStack.setItemDamage(itemStack.getItemDamage()+1);
		return itemStack;
	}
	
	
}
