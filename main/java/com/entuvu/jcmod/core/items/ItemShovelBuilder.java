package com.entuvu.jcmod.core.items;

import com.entuvu.jcmod.JCMain;

import net.minecraft.item.ItemSpade;

public class ItemShovelBuilder extends ItemSpade{
	public ItemShovelBuilder(String unlocalizedName, ToolMaterial material) {
		super(material);
		this.setCreativeTab(JCMain.tabStandard);
		this.setUnlocalizedName(unlocalizedName);
	}
}
