package com.entuvu.jcmod.core.items;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.blocks.BlockTopsoil;
import com.entuvu.jcmod.core.register.RegisterBlock;
import com.entuvu.jcmod.core.register.RegisterItem;

import net.minecraft.block.BlockFarmland;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class ItemFertilizer extends Item {
	
	public ItemFertilizer(String unlocalizedName) {
		super();
		this.setCreativeTab(JCMain.tabStandard);
		this.setUnlocalizedName(unlocalizedName);
		this.setMaxDamage(3);
		this.isDamageable();
		this.setContainerItem(RegisterItem.burlap_sack);
	}
	
	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side,
			float hitX, float hitY, float hitZ) {
		if(worldIn.getBlockState(pos).getBlock() instanceof BlockFarmland &&
				!(worldIn.getBlockState(pos).getBlock() instanceof BlockTopsoil)){
			worldIn.setBlockState(pos, RegisterBlock.topsoil.getDefaultState());
			stack.damageItem(1, playerIn);
			if(stack.getItemDamage() >= stack.getMaxDamage()){
				stack.setItem(RegisterItem.burlap_sack);
			}
		}
		return super.onItemUse(stack, playerIn, worldIn, pos, side, hitX, hitY, hitZ);
	}
	
	
}
