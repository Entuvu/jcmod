package com.entuvu.jcmod.core.items;

import com.entuvu.jcmod.JCMain;

import net.minecraft.item.ItemPickaxe;

public class ItemPickBuilder extends ItemPickaxe {
	public ItemPickBuilder(String unlocalizedName, ToolMaterial material) {
		super(material);
		this.setCreativeTab(JCMain.tabStandard);
		this.setUnlocalizedName(unlocalizedName);
	}
}
