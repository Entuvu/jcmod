package com.entuvu.jcmod.core.items;

import com.entuvu.jcmod.JCMain;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class ItemFoodBuilder extends ItemFood{
	private PotionEffect[] effects;

	public ItemFoodBuilder(String unlocalizedName, int amount, float saturation, boolean isWolfFood, PotionEffect... effects) {
		super(amount, saturation, isWolfFood);
		this.setCreativeTab(JCMain.tabStandard);
		this.setUnlocalizedName(unlocalizedName);
		this.effects = effects;
	}
	
	@Override
	protected void onFoodEaten(ItemStack stack, World worldIn, EntityPlayer player) {
		super.onFoodEaten(stack, worldIn, player);
		for(int i = 0; i < effects.length; i ++){
			if(!worldIn.isRemote && effects[i] != null && effects[i].getPotionID() > 0){
				player.addPotionEffect(new PotionEffect(this.effects[i]));
			}
		}
	}

}
