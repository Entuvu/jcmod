package com.entuvu.jcmod.core.entity.projectile;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityRock extends EntityThrowable {

	public EntityRock(World worldIn)
    {
        super(worldIn);
    }

    public EntityRock(World worldIn, EntityLivingBase p_i1774_2_)
    {
        super(worldIn, p_i1774_2_);
    }

    public EntityRock(World worldIn, double x, double y, double z)
    {
        super(worldIn, x, y, z);
    }

	@Override
	protected void onImpact(MovingObjectPosition p_70184_1_) {
		 if (p_70184_1_.entityHit != null)
	        {
	            byte b0 = 1;

	            p_70184_1_.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), (float)b0);
	        }

	        for (int i = 0; i < 8; ++i)
	        {
	            this.worldObj.spawnParticle(EnumParticleTypes.SNOWBALL, this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D, new int[0]);
	        }
	        
	        

	        if (!this.worldObj.isRemote)
	        {
	        	// Was looking into spawning rock into world sometimes.  Look at arrow //
	        	
	            this.setDead();
	        }
	}

}
