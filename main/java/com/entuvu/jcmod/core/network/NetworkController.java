package com.entuvu.jcmod.core.network;

import com.entuvu.jcmod.JCMain;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class NetworkController {
	public static SimpleNetworkWrapper network;
	
	public static void preInit() {
		network = NetworkRegistry.INSTANCE.newSimpleChannel("jcchannel");
		network.registerMessage(PacketSmeltingFurnace.PacketHandler.class, PacketSmeltingFurnace.class, 0, Side.CLIENT); 
		network.registerMessage(PacketCampFire.PacketHandler.class, PacketCampFire.class, 1, Side.CLIENT);
		network.registerMessage(PacketAlloyFurnace.PacketHandler.class, PacketAlloyFurnace.class, 2, Side.CLIENT);
	}

	public static void init() {
		NetworkRegistry.INSTANCE.registerGuiHandler(JCMain.instance, new JCGuiHandler());
    	
	}
}
