package com.entuvu.jcmod.core.network;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.tileentity.TileEntityAlloyFurnace;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketAlloyFurnace implements IMessage{
	
	BlockPos pos;
	
	int leftBurnTime;
	int leftItemBurnTime;
	int rightBurnTime;
	int rightItemBurnTime;
	int currentSmeltingTime;
	int totalSmeltingTime;
	
	public static class PacketHandler implements IMessageHandler<PacketAlloyFurnace, IMessage>{

		@Override
		public IMessage onMessage(PacketAlloyFurnace message, MessageContext ctx) {
			EntityPlayer player = JCMain.proxy.getPlayerFromMessageContext(ctx);
			TileEntityAlloyFurnace alloyFurnace = (TileEntityAlloyFurnace)player.worldObj.getTileEntity(message.pos);

			if(alloyFurnace != null){
				alloyFurnace.setField(0, message.leftBurnTime);
				alloyFurnace.setField(1, message.leftItemBurnTime);
				alloyFurnace.setField(2, message.rightBurnTime);
				alloyFurnace.setField(3, message.rightItemBurnTime);
				alloyFurnace.setField(4, message.currentSmeltingTime);
				alloyFurnace.setField(5, message.totalSmeltingTime);
			}
			return null;
		}
		
	}
	
	public PacketAlloyFurnace(){
		pos = new BlockPos(0,0,0);
		totalSmeltingTime = 0 ;
	}
	
	public PacketAlloyFurnace(BlockPos loc, int leftBurnTime, int leftItemBurnTime, int rightBurnTime, int rightItemBurnTime, int currentSmeltingTime, int totalSmeltingTime) {
		pos = loc;
		this.leftBurnTime = leftBurnTime;
		this.leftItemBurnTime = leftItemBurnTime;
		this.rightBurnTime = rightBurnTime;
		this.rightItemBurnTime = rightItemBurnTime;
		this.currentSmeltingTime = currentSmeltingTime;
		this.totalSmeltingTime = totalSmeltingTime;
	}
	

	@Override
	public void fromBytes(ByteBuf buf) {
		pos = new BlockPos(buf.readInt(), buf.readInt(), buf.readInt());
		leftBurnTime = buf.readInt();
		leftItemBurnTime = buf.readInt();
		rightBurnTime = buf.readInt();
		rightItemBurnTime = buf.readInt();
		currentSmeltingTime = buf.readInt();
		totalSmeltingTime = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(pos.getX());
		buf.writeInt(pos.getY());
		buf.writeInt(pos.getZ());
		
		buf.writeInt(leftBurnTime);
		buf.writeInt(leftItemBurnTime);
		buf.writeInt(rightBurnTime);
		buf.writeInt(rightItemBurnTime);
		buf.writeInt(currentSmeltingTime);
		buf.writeInt(totalSmeltingTime);
	}

}
