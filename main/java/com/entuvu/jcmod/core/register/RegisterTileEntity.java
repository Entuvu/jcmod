package com.entuvu.jcmod.core.register;

import com.entuvu.jcmod.core.testing.TileEntityTestMixer;
import com.entuvu.jcmod.core.tileentity.TileEntityAlloyFurnace;
import com.entuvu.jcmod.core.tileentity.TileEntityCampFire;
import com.entuvu.jcmod.core.tileentity.TileEntityCrate;
import com.entuvu.jcmod.core.tileentity.TileEntityMixer;
import com.entuvu.jcmod.core.tileentity.TileEntitySmeltingFurnace;
import com.entuvu.jcmod.core.tileentity.TileEntityThatchBasket;

import net.minecraftforge.fml.common.registry.GameRegistry;

public class RegisterTileEntity {
	public static void createTileEntities(){
			GameRegistry.registerTileEntity(TileEntityCrate.class, "jcmod_crate");
			GameRegistry.registerTileEntity(TileEntityCampFire.class, "jcmod_camp_fire");
			GameRegistry.registerTileEntity(TileEntityThatchBasket.class, "jcmod_thatch_basket");
			GameRegistry.registerTileEntity(TileEntityAlloyFurnace.class, "jcmod_alloy_furnace");
			GameRegistry.registerTileEntity(TileEntitySmeltingFurnace.class, "jcmod_smelting_furnace");
			GameRegistry.registerTileEntity(TileEntityMixer.class, "jcmod_mixer");
			
			GameRegistry.registerTileEntity(TileEntityTestMixer.class, "jcmod_test_mixer");
	}
}
