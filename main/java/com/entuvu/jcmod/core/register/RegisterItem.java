package com.entuvu.jcmod.core.register;

import com.entuvu.jcmod.JCMain;
import com.entuvu.jcmod.core.items.ItemArmorBuilder;
import com.entuvu.jcmod.core.items.ItemAxeBuilder;
import com.entuvu.jcmod.core.items.ItemBasicBuilder;
import com.entuvu.jcmod.core.items.ItemFertilizer;
import com.entuvu.jcmod.core.items.ItemFoodBuilder;
import com.entuvu.jcmod.core.items.ItemHoeBuilder;
import com.entuvu.jcmod.core.items.ItemPickBuilder;
import com.entuvu.jcmod.core.items.ItemRock;
import com.entuvu.jcmod.core.items.ItemShovelBuilder;
import com.entuvu.jcmod.core.items.ItemSwordBuilder;

import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemSeeds;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;

public final class RegisterItem {
		// JC Mod Ingots //
		public static Item bronze_ingot;
		public static Item copper_ingot;
		public static Item lead_ingot;
		public static Item silver_ingot;
		public static Item steel_ingot;
		public static Item tin_ingot;
		
		// Molds //
		public static Item clay_ingot_mold;
		public static Item clay_bar_mold;
		public static Item clay_nail_mold;
		public static Item clay_arrowhead_mold;
		public static Item clay_hinge_mold;
		public static Item ingot_mold;
		public static Item bar_mold;
		public static Item nail_mold;
		public static Item arrowhead_mold;
		public static Item hinge_mold;
		
		// Other Metal items //
		public static Item bronze_sheet;
		public static Item copper_sheet;
		public static Item gold_sheet;
		public static Item tin_sheet;
		public static Item copper_nugget;
		public static Item gold_nugget;
		public static Item iron_nugget;
		public static Item tin_nugget;
		public static Item tin_can;
		public static Item copper_bar;
		public static Item gold_bar;
		public static Item iron_bar;
		public static Item tin_bar;
		public static Item iron_nails;
		public static Item copper_arrowhead;
		public static Item iron_hinge;
		
		// Metal Chunks //
		public static Item tin_chunk;
		public static Item copper_chunk;
		public static Item gold_chunk;
		public static Item iron_chunk;
		public static Item lead_chunk;
		public static Item silver_chunk;
		
		// Material ROCKS //
		public static Item rock;
		public static Item rock_andesite;
		public static Item rock_diorite;
		public static Item rock_granite;		
		public static Item brick_sand;
		public static Item brick_stone;
		
		// Material Cut //
		//public static Item wood_plank;
		public static Item wood_sheet;
		public static Item plank_oak;
		public static Item plank_birch;
		public static Item plank_acacia;
		public static Item plank_jungle;
		public static Item plank_spruce;
		public static Item plank_darkoak;
		
		// Materials Natural //
		public static Item branch;
		public static Item thatch;
		public static Item twine;
		public static Item jute;
		public static Item mortar;
		public static Item bucket_mortar;
		public static Item compost;
		public static Item bucket_compost;
		public static Item fertilizer;
		public static Item sack_fertilizer;		
		public static Item burlap_sack;
		public static Item clay_shards;
		public static Item flint_shards;
		public static Item quartz_shards;
		public static Item bone_shards;
		public static Item pile_gravel;
		public static Item pile_sand;
		public static Item pile_red_sand;
		public static Item pile_dirt;
		
		// Material FIRE //
		public static Item kindling;
		
		// Material Tools //
		public static Item firestarter;
		public static Item wood_handle;
		public static Item wood_sieve;

		public static Item stone_pickaxe_head;
		public static Item stone_hoe_head;		
		public static Item stone_shovel_head;
		public static Item stone_axe_head;
		/*public static Item copper_sword_hilt;
		public static Item copper_sword_blade;
		public static Item copper_pick_head;
		public static Item copper_hoe_head;
		public static Item copper_shovel_head;
		public static Item copper_axe_head;*/
		public static Item copper_saw_blade;
		public static Item bronze_sword_hilt;
		public static Item bronze_sword_blade;
		public static Item bronze_pick_head;
		public static Item bronze_hoe_head;
		public static Item bronze_shovel_head;
		public static Item bronze_axe_head;
		//public static Item bronze_saw_blade;
		public static Item brick_red_sand;
		public static Item iron_hammer_head;
		public static Item iron_sword_hilt;
		public static Item iron_sword_blade;
		public static Item iron_pick_head;
		public static Item iron_hoe_head;
		public static Item iron_shovel_head;
		public static Item iron_axe_head;
		public static Item iron_saw_blade;
		
		// Tools Picks //
		//public static Item copper_pick;
		public static Item bronze_pick;

		// Tools Axes //
		public static Item flint_axe;
		//public static Item copper_axe;
		public static Item bronze_axe;
		
		// Tools Hoes //
		//public static Item copper_hoe;
		public static Item bronze_hoe;
	
		// Tools Shovel //
		//public static Item copper_shovel;
		public static Item bronze_shovel;
		
		// Tools Weapons //
		public static Item flint_knife;
		public static Item copper_knife;
		public static Item iron_knife;
		public static Item wood_spear;
		public static Item copper_spear;
		//public static Item copper_sword;
		public static Item bronze_sword;
		
		// Custom Tools //
		public static Item copper_saw;
		public static Item copper_chisel;
		//public static Item bronze_saw;
		//public static Item bronze_chisel;
		public static Item iron_hammer;
		public static Item iron_saw;
		public static Item iron_chisel;
		
		// Armor //
		public static Item grass_chestplate;
		public static Item grass_helmet;
		public static Item grass_leggings;
		public static Item grass_boots;
		
		// Food //
		public static Item chocolate_bar;
		public static Item grub;
		public static Item boiled_egg;
		
		// Misc. Items //
		public static Item jc_book;
		public static Item can_grubs;
		
		// Others NOT FOR FINAL GAME //
		public static Item oven_mitt;
		public static Item meta_item;
		public static Item mod_sword;
		
		/* plants */
		public static Item jute_seed;
	
	public static void createItems(){
		// Realism Ingots //
		GameRegistry.registerItem(bronze_ingot = new ItemBasicBuilder("bronze_ingot"), "bronze_ingot");
		GameRegistry.registerItem(copper_ingot = new ItemBasicBuilder("copper_ingot"), "copper_ingot");
		GameRegistry.registerItem(lead_ingot = new ItemBasicBuilder("lead_ingot"), "lead_ingot");
		GameRegistry.registerItem(silver_ingot = new ItemBasicBuilder("silver_ingot"), "silver_ingot");
		GameRegistry.registerItem(steel_ingot = new ItemBasicBuilder("steel_ingot"), "steel_ingot");
		GameRegistry.registerItem(tin_ingot = new ItemBasicBuilder("tin_ingot"), "tin_ingot");
		
		// Molds //
		GameRegistry.registerItem(clay_ingot_mold = new ItemBasicBuilder("clay_ingot_mold"), "clay_ingot_mold");
		GameRegistry.registerItem(clay_bar_mold = new ItemBasicBuilder("clay_bar_mold"), "clay_bar_mold");
		GameRegistry.registerItem(clay_nail_mold = new ItemBasicBuilder("clay_nail_mold"), "clay_nail_mold");
		GameRegistry.registerItem(clay_arrowhead_mold = new ItemBasicBuilder("clay_arrowhead_mold"), "clay_arrowhead_mold");
		GameRegistry.registerItem(clay_hinge_mold = new ItemBasicBuilder("clay_hinge_mold"), "clay_hinge_mold");
		GameRegistry.registerItem(ingot_mold = new ItemBasicBuilder("ingot_mold"), "ingot_mold");
		GameRegistry.registerItem(bar_mold = new ItemBasicBuilder("bar_mold"), "bar_mold");
		GameRegistry.registerItem(nail_mold = new ItemBasicBuilder("nail_mold"), "nail_mold");
		GameRegistry.registerItem(arrowhead_mold = new ItemBasicBuilder("arrowhead_mold"), "arrowhead_mold");
		GameRegistry.registerItem(hinge_mold = new ItemBasicBuilder("hinge_mold"), "hinge_mold");
		
		// Other Metal items //
		GameRegistry.registerItem(bronze_sheet = new ItemBasicBuilder("bronze_sheet"), "bronze_sheet");
		GameRegistry.registerItem(copper_sheet = new ItemBasicBuilder("copper_sheet"), "copper_sheet");
		GameRegistry.registerItem(gold_sheet = new ItemBasicBuilder("gold_sheet"), "gold_sheet");
		GameRegistry.registerItem(tin_sheet = new ItemBasicBuilder("tin_sheet"), "tin_sheet");
		GameRegistry.registerItem(copper_nugget = new ItemBasicBuilder("copper_nugget"), "copper_nugget");
		GameRegistry.registerItem(gold_nugget = new ItemBasicBuilder("gold_nugget"), "gold_nugget");
		GameRegistry.registerItem(iron_nugget = new ItemBasicBuilder("iron_nugget"), "iron_nugget");
		GameRegistry.registerItem(tin_nugget = new ItemBasicBuilder("tin_nugget"), "tin_nugget");
		GameRegistry.registerItem(tin_can = new ItemBasicBuilder("tin_can"), "tin_can");
		GameRegistry.registerItem(copper_bar = new ItemBasicBuilder("copper_bar"), "copper_bar");
		GameRegistry.registerItem(gold_bar = new ItemBasicBuilder("gold_bar"), "gold_bar");
		GameRegistry.registerItem(iron_bar = new ItemBasicBuilder("iron_bar"), "iron_bar");
		GameRegistry.registerItem(tin_bar = new ItemBasicBuilder("tin_bar"), "tin_bar");
		GameRegistry.registerItem(iron_nails = new ItemBasicBuilder("iron_nails"), "iron_nails");
		GameRegistry.registerItem(copper_arrowhead = new ItemBasicBuilder("copper_arrowhead"), "copper_arrowhead");
		GameRegistry.registerItem(iron_hinge = new ItemBasicBuilder("iron_hinge"), "iron_hinge");
		
		// Material Chunk //
		GameRegistry.registerItem(tin_chunk = new ItemBasicBuilder("tin_chunk"), "tin_chunk");
		GameRegistry.registerItem(copper_chunk = new ItemBasicBuilder("copper_chunk"), "copper_chunk");
		GameRegistry.registerItem(gold_chunk = new ItemBasicBuilder("gold_chunk"), "gold_chunk");
		GameRegistry.registerItem(iron_chunk = new ItemBasicBuilder("iron_chunk"), "iron_chunk");
		GameRegistry.registerItem(lead_chunk = new ItemBasicBuilder("lead_chunk"), "lead_chunk");
		GameRegistry.registerItem(silver_chunk = new ItemBasicBuilder("silver_chunk"), "silver_chunk");
		
		// Material ROCKS //
		GameRegistry.registerItem(rock = new ItemRock("rock"), "rock");
		GameRegistry.registerItem(rock_andesite = new ItemRock("rock_andesite"), "rock_andesite");
		GameRegistry.registerItem(rock_diorite = new ItemRock("rock_diorite"), "rock_diorite");
		GameRegistry.registerItem(rock_granite = new ItemRock("rock_granite"), "rock_granite");		
		GameRegistry.registerItem(brick_sand = new ItemBasicBuilder("brick_sand"), "brick_sand");
		GameRegistry.registerItem(brick_red_sand = new ItemBasicBuilder("brick_red_sand"), "brick_red_sand");
		GameRegistry.registerItem(brick_stone = new ItemBasicBuilder("brick_stone"), "brick_stone");
		
		// Material Cut //
		//GameRegistry.registerItem(wood_plank = new ItemBasicBuilder("wood_plank"), "wood_plank");
		GameRegistry.registerItem(wood_sheet = new ItemBasicBuilder("wood_sheet"), "wood_sheet");
		GameRegistry.registerItem(plank_oak = new ItemBasicBuilder("plank_oak"), "plank_oak");
		GameRegistry.registerItem(plank_birch = new ItemBasicBuilder("plank_birch"), "plank_birch");
		GameRegistry.registerItem(plank_acacia = new ItemBasicBuilder("plank_acacia"), "plank_acacia");
		GameRegistry.registerItem(plank_jungle = new ItemBasicBuilder("plank_jungle"), "plank_jungle");
		GameRegistry.registerItem(plank_spruce = new ItemBasicBuilder("plank_spruce"), "plank_spruce");
		GameRegistry.registerItem(plank_darkoak = new ItemBasicBuilder("plank_darkoak"), "plank_darkoak");		
		
		// Materials Natural //
		GameRegistry.registerItem(branch = new ItemBasicBuilder("branch"), "branch");
		GameRegistry.registerItem(thatch = new ItemBasicBuilder("thatch"), "thatch");
		GameRegistry.registerItem(twine = new ItemBasicBuilder("twine"), "twine");
		GameRegistry.registerItem(mortar = new ItemBasicBuilder("mortar"), "mortar");
		GameRegistry.registerItem(bucket_mortar = new ItemBasicBuilder("bucket_mortar"), "bucket_mortar");
		GameRegistry.registerItem(compost = new ItemBasicBuilder("compost"), "compost");
		GameRegistry.registerItem(bucket_compost = new ItemBasicBuilder("bucket_compost"), "bucket_compost");
		GameRegistry.registerItem(fertilizer = new ItemFertilizer("fertilizer"), "fertilizer");
		GameRegistry.registerItem(sack_fertilizer = new ItemFertilizer("sack_fertilizer"), "sack_fertilizer");
		GameRegistry.registerItem(clay_shards = new ItemBasicBuilder("clay_shards"), "clay_shards");
		GameRegistry.registerItem(flint_shards = new ItemBasicBuilder("flint_shards"), "flint_shards");
		GameRegistry.registerItem(quartz_shards = new ItemBasicBuilder("quartz_shards"), "quartz_shards");
		GameRegistry.registerItem(bone_shards = new ItemBasicBuilder("bone_shards"), "bone_shards");
		GameRegistry.registerItem(pile_gravel = new ItemBasicBuilder("pile_gravel"), "pile_gravel");
		GameRegistry.registerItem(pile_sand = new ItemBasicBuilder("pile_sand"), "pile_sand");
		GameRegistry.registerItem(pile_red_sand = new ItemBasicBuilder("pile_red_sand"), "pile_red_sand");
		GameRegistry.registerItem(pile_dirt = new ItemBasicBuilder("pile_dirt"), "pile_dirt");
		GameRegistry.registerItem(jute = new ItemBasicBuilder("jute"), "jute");
		GameRegistry.registerItem(burlap_sack = new ItemBasicBuilder("burlap_sack"), "burlap_sack");
		
		// Material FIRE //
		GameRegistry.registerItem(kindling = new ItemBasicBuilder("kindling"), "kindling");
		
		// Material Tools //
		GameRegistry.registerItem(firestarter = new ItemBasicBuilder("firestarter", 25), "firestarter");
		GameRegistry.registerItem(wood_handle=new ItemBasicBuilder("wood_handle"), "wood_handle");
		GameRegistry.registerItem(wood_sieve = new ItemBasicBuilder("sieve_wood"), "sieve_wood");
		GameRegistry.registerItem(stone_pickaxe_head = new ItemBasicBuilder("stone_pickaxe_head"), "stone_pickaxe_head");
		GameRegistry.registerItem(stone_hoe_head = new ItemBasicBuilder("stone_hoe_head"), "stone_hoe_head");
		GameRegistry.registerItem(stone_shovel_head = new ItemBasicBuilder("stone_shovel_head"), "stone_shovel_head");
		GameRegistry.registerItem(stone_axe_head = new ItemBasicBuilder("stone_axe_head"), "stone_axe_head");		
		/*GameRegistry.registerItem(copper_sword_hilt = new ItemBasicBuilder("copper_sword_hilt"), "copper_sword_hilt");
		GameRegistry.registerItem(copper_sword_blade = new ItemBasicBuilder("copper_sword_blade"), "copper_sword_blade");
		GameRegistry.registerItem(copper_pick_head = new ItemBasicBuilder("copper_pick_head"), "copper_pick_head");
		GameRegistry.registerItem(copper_hoe_head = new ItemBasicBuilder("copper_hoe_head"), "copper_hoe_head");
		GameRegistry.registerItem(copper_shovel_head = new ItemBasicBuilder("copper_shovel_head"), "copper_shovel_head");
		GameRegistry.registerItem(copper_axe_head = new ItemBasicBuilder("copper_axe_head"), "copper_axe_head");*/
		GameRegistry.registerItem(copper_saw_blade = new ItemBasicBuilder("copper_saw_blade"), "copper_saw_blade");
		GameRegistry.registerItem(bronze_sword_hilt = new ItemBasicBuilder("bronze_sword_hilt"), "bronze_sword_hilt");
		GameRegistry.registerItem(bronze_sword_blade = new ItemBasicBuilder("bronze_sword_blade"), "bronze_sword_blade");
		GameRegistry.registerItem(bronze_pick_head = new ItemBasicBuilder("bronze_pick_head"), "bronze_pick_head");
		GameRegistry.registerItem(bronze_hoe_head = new ItemBasicBuilder("bronze_hoe_head"), "bronze_hoe_head");
		GameRegistry.registerItem(bronze_shovel_head = new ItemBasicBuilder("bronze_shovel_head"), "bronze_shovel_head");
		GameRegistry.registerItem(bronze_axe_head = new ItemBasicBuilder("bronze_axe_head"), "bronze_axe_head");
		//GameRegistry.registerItem(bronze_saw_blade = new ItemBasicBuilder("bronze_saw_blade"), "bronze_saw_blade");	
		GameRegistry.registerItem(iron_hammer_head = new ItemBasicBuilder("iron_hammer_head"), "iron_hammer_head");
		GameRegistry.registerItem(iron_sword_hilt = new ItemBasicBuilder("iron_sword_hilt"), "iron_sword_hilt");
		GameRegistry.registerItem(iron_sword_blade = new ItemBasicBuilder("iron_sword_blade"), "iron_sword_blade");
		GameRegistry.registerItem(iron_pick_head = new ItemBasicBuilder("iron_pick_head"), "iron_pick_head");
		GameRegistry.registerItem(iron_hoe_head = new ItemBasicBuilder("iron_hoe_head"), "iron_hoe_head");
		GameRegistry.registerItem(iron_shovel_head = new ItemBasicBuilder("iron_shovel_head"), "iron_shovel_head");
		GameRegistry.registerItem(iron_axe_head = new ItemBasicBuilder("iron_axe_head"), "iron_axe_head");
		GameRegistry.registerItem(iron_saw_blade = new ItemBasicBuilder("iron_saw_blade"), "iron_saw_blade");	
		
		// Tools Picks //
		//GameRegistry.registerItem(copper_pick = new ItemPickBuilder("copper_pick", COPPER), "copper_pick");
		GameRegistry.registerItem(bronze_pick = new ItemPickBuilder("bronze_pick", BRONZE), "bronze_pick");
		
		// Tools Axes //
		GameRegistry.registerItem(flint_axe = new ItemAxeBuilder("flint_axe", AXE_FLINT), "flint_axe");
		//GameRegistry.registerItem(copper_axe = new ItemAxeBuilder("copper_axe", COPPER), "copper_axe");
		GameRegistry.registerItem(bronze_axe = new ItemAxeBuilder("bronze_axe", BRONZE), "bronze_axe");
		
		// Tools Hoes //
		//GameRegistry.registerItem(copper_hoe = new ItemHoeBuilder("copper_hoe", COPPER), "copper_hoe");
		GameRegistry.registerItem(bronze_hoe = new ItemHoeBuilder("bronze_hoe", BRONZE), "bronze_hoe");
		
		// Tools Shovel //
		//GameRegistry.registerItem(copper_shovel = new ItemShovelBuilder("copper_shovel", COPPER), "copper_shovel");
		GameRegistry.registerItem(bronze_shovel = new ItemShovelBuilder("bronze_shovel", BRONZE), "bronze_shovel");
		
		// Tools Weapons //
		GameRegistry.registerItem(flint_knife = new ItemSwordBuilder("flint_knife", FLINT, 40), "flint_knife");
		GameRegistry.registerItem(copper_knife = new ItemSwordBuilder("copper_knife", COPPER, 60), "copper_knife");
		GameRegistry.registerItem(iron_knife = new ItemSwordBuilder("iron_knife", KNIFE_IRON, 100), "iron_knife");
		GameRegistry.registerItem(wood_spear = new ItemSwordBuilder("wood_spear", SPEAR), "wood_spear");
		GameRegistry.registerItem(copper_spear = new ItemSwordBuilder("copper_spear", SPEAR_COPPER), "copper_spear");
		//GameRegistry.registerItem(copper_sword = new ItemSwordBuilder("copper_sword", COPPER), "copper_sword");
		GameRegistry.registerItem(bronze_sword = new ItemSwordBuilder("bronze_sword", BRONZE), "bronze_sword");
		
		// Custom Tools //
		GameRegistry.registerItem(copper_saw = new ItemBasicBuilder("copper_saw", 50), "copper_saw");
		GameRegistry.registerItem(copper_chisel = new ItemBasicBuilder("copper_chisel", 50), "copper_chisel");
		//GameRegistry.registerItem(bronze_saw = new ItemBasicBuilder("bronze_saw", 75), "bronze_saw");
		//GameRegistry.registerItem(bronze_chisel = new ItemBasicBuilder("bronze_chisel", 75), "bronze_chisel");
		GameRegistry.registerItem(iron_hammer = new ItemSwordBuilder("iron_hammer", HAMMER_IRON, 75), "iron_hammer");
		GameRegistry.registerItem(iron_saw = new ItemBasicBuilder("iron_saw", 125), "iron_saw");
		GameRegistry.registerItem(iron_chisel = new ItemBasicBuilder("iron_chisel", 125), "iron_chisel");
		
		// Armor //
		GameRegistry.registerItem(grass_helmet = new ItemArmorBuilder("grass_helmet", GRASS, 1, 0), "grass_helmet");
		GameRegistry.registerItem(grass_chestplate = new ItemArmorBuilder("grass_chestplate", GRASS, 1, 1), "grass_chestplate");
		GameRegistry.registerItem(grass_leggings = new ItemArmorBuilder("grass_leggings", GRASS, 2, 2), "grass_leggings");
		GameRegistry.registerItem(grass_boots = new ItemArmorBuilder("grass_boots", GRASS, 1, 3), "grass_boots");
		
		// Food //
		GameRegistry.registerItem(chocolate_bar = new ItemFoodBuilder("chocolate_bar", 2, 0.2f, false,new PotionEffect(Potion.moveSpeed.id, 1200, 1), new PotionEffect(Potion.nightVision.id, 600, 0)).setAlwaysEdible(), "chocolate_bar");
		GameRegistry.registerItem(grub = new ItemFoodBuilder("grub", 1, 0.5f, false), "grub");
		GameRegistry.registerItem(boiled_egg = new ItemFoodBuilder("boiled_egg", 1, 2.0f, false), "boiled_egg");
		
		// Misc. Items //
		GameRegistry.registerItem(jc_book = new ItemBasicBuilder("jc_book"), "jc_book");
		GameRegistry.registerItem(can_grubs = new ItemBasicBuilder("can_grubs"), "can_grubs");
		
		/* plants */
		GameRegistry.registerItem(jute_seed = new ItemSeeds(RegisterBlock.jute_plant, Blocks.farmland).setCreativeTab(JCMain.tabStandard).setUnlocalizedName("jute_seed"), "jute_seed");
	}
		
	// harvestLevel, maxUses, efficiency, damageVsEntity, enchantability
	public static ToolMaterial FLINT = EnumHelper.addToolMaterial("Flint", 0, 30, 1f, -1.5f, 5);
	public static ToolMaterial AXE_FLINT = EnumHelper.addToolMaterial("Axe_flint", 0, 60, 1f, 0.0f, 5);
	public static ToolMaterial SPEAR = EnumHelper.addToolMaterial("Spear", 0, 20, 1f, -0.5f, 5);
	public static ToolMaterial SPEAR_COPPER = EnumHelper.addToolMaterial("Spear_copper", 0, 50, 1f, 1.0f, 5);
	public static ArmorMaterial GRASS = EnumHelper.addArmorMaterial("Grass", "jcmod:grass", 3, new int[]{1,2,2,1}, 10);
	public static ToolMaterial COPPER = EnumHelper.addToolMaterial("Copper", 1, 155, 1f, -0.0f, 10);
	public static ToolMaterial BRONZE = EnumHelper.addToolMaterial("Bronze", 2, 190, 1f, 1.5f, 10);
	public static ToolMaterial STEEL = EnumHelper.addToolMaterial("Steel", 2, 500, 1f, 2.5f, 10);
	public static ToolMaterial HAMMER_IRON = EnumHelper.addToolMaterial("Hammer_iron", 2, 75, 1f, 1.5f, 10);
	public static ToolMaterial KNIFE_IRON = EnumHelper.addToolMaterial("Knife_iron", 2, 75, 1f, -0.5f, 10);
	
}
