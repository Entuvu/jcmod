package com.entuvu.jcmod.core.register;

import com.entuvu.jcmod.core.blocks.BlockJute;
import com.entuvu.jcmod.core.blocks.BlockSod;
import com.entuvu.jcmod.core.blocks.BlockThatchBlock;
import com.entuvu.jcmod.core.blocks.BlockTopsoil;
import com.entuvu.jcmod.core.blocks.BlockWallLogLeft;
import com.entuvu.jcmod.core.blocks.BlockWallLogMiddle;
import com.entuvu.jcmod.core.blocks.BlockWallLogRight;
import com.entuvu.jcmod.core.blocks.BlockWoodFrame;
import com.entuvu.jcmod.core.blocks.BlockWoodStone;
import com.entuvu.jcmod.core.blocks.BlockWoodStoneFancy;
import com.entuvu.jcmod.core.blocks.BlockWoodStoneH;
import com.entuvu.jcmod.core.blocks.BlockWoodStoneL;
import com.entuvu.jcmod.core.blocks.BlockWoodStoneNoBottom;
import com.entuvu.jcmod.core.blocks.BlockWoodStoneNoLeft;
import com.entuvu.jcmod.core.blocks.BlockWoodStoneNoRight;
import com.entuvu.jcmod.core.blocks.BlockWoodStoneNoTop;
import com.entuvu.jcmod.core.blocks.BlockWoodStoneR;
import com.entuvu.jcmod.core.blocks.BlockWoodStoneV;
import com.entuvu.jcmod.core.blocks.builders.BlockSpecialBuilder;
import com.entuvu.jcmod.core.blocks.builders.BlockStairsBuilder;
import com.entuvu.jcmod.core.blocks.builders.SpecialBlocks;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodPalisade;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodPalisadeBottom;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodPalisadeTop;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatAcaciaBottom;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatAcaciaMiddle;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatAcaciaTop;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatBirchBottom;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatBirchMiddle;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatBirchTop;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatBottom;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatJungleBottom;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatJungleMiddle;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatJungleTop;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatMiddle;
import com.entuvu.jcmod.core.blocks.typeDynamic.BlockWoodSlatTop;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockAlloyFurnace;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockArtisanBench;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockBrickOven;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockButchersTable;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockCampFire;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockCrate;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockMixer;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockSmeltingFurnace;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockStonecuttersBench;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockThatchBasket;
import com.entuvu.jcmod.core.blocks.typeEntity.BlockWoodStump;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.registry.GameRegistry;

public final class RegisterBlock {
	/* ores */
	public static Block tin_ore;
	public static Block copper_ore;
	public static Block lead_ore;
	public static Block silver_ore;
	
	/* stone */ 
	public static Block cobblestone_andesite;
	public static Block cobblestone_diorite;
	public static Block cobblestone_granite;
	
	/* stairs */
	public static Block stairs_thatch;
	public static Block stairs_andesite;
	public static Block stairs_diorite;
	public static Block stairs_granite;
	
	/* slabs */
	public static Block slab_andesite;
	public static Block slab_diorite;
	public static Block slab_granite;
	
	/* other decorative */
	public static Block thatch_block;
	public static Block sod;
	
	/* dynamic */
	public static Block wood_palisade_bottom;
	public static Block wood_palisade_top;
	public static Block wood_palisade;
	public static Block wall_log_left;
	public static Block wall_log_middle;
	public static Block wall_log_right;
	/* wood */
	public static Block wood_frame;
	public static Block wood_slat_bottom;
	public static Block wood_slat_middle;
	public static Block wood_slat_top;
	public static Block wood_slat_birch_bottom;
	public static Block wood_slat_birch_middle;
	public static Block wood_slat_birch_top;
	public static Block wood_slat_acacia_bottom;
	public static Block wood_slat_acacia_middle;
	public static Block wood_slat_acacia_top;
	public static Block wood_slat_jungle_bottom;
	public static Block wood_slat_jungle_middle;
	public static Block wood_slat_jungle_top;
	/* wood stone */
	public static Block wood_stone;
	public static Block wood_stone_fancy;
	public static Block wood_stone_r;
	public static Block wood_stone_l;
	public static Block wood_stone_h;
	public static Block wood_stone_v;
	public static Block wood_stone_no_top;
	public static Block wood_stone_no_bottom;
	public static Block wood_stone_no_left;
	public static Block wood_stone_no_right;
	
	/* tileentity blocks */
	public static Block alloy_furnace;
	public static Block smelting_furnace;
	public static Block mixer;
	public static Block brick_oven;
	public static Block wood_stump;	
	public static Block butchers_table;	
	public static Block artisan_bench;
	public static Block crate;
	public static Block thatch_basket;
	public static Block camp_fire;
	public static Block stonecutters_bench;
	
	/* special purpose blocks */
	public static Block topsoil;
	
	/* plants */
	public static Block jute_plant;

	public static void createBlocks(){
		/* ores */
		GameRegistry.registerBlock(tin_ore = new BlockSpecialBuilder("tin_ore", SpecialBlocks.TIN_ORE), "tin_ore");
		GameRegistry.registerBlock(lead_ore = new BlockSpecialBuilder("lead_ore", SpecialBlocks.LEAD_ORE), "lead_ore");
		GameRegistry.registerBlock(silver_ore = new BlockSpecialBuilder("silver_ore", SpecialBlocks.SILVER_ORE), "silver_ore");
		GameRegistry.registerBlock(copper_ore = new BlockSpecialBuilder("copper_ore", SpecialBlocks.COPPER_ORE), "copper_ore");
		
		/* stone */
		GameRegistry.registerBlock(cobblestone_andesite = new BlockSpecialBuilder("cobblestone_andesite", SpecialBlocks.ANDESITE_COBBLESTONE), "cobblestone_andesite");
		GameRegistry.registerBlock(cobblestone_diorite = new BlockSpecialBuilder("cobblestone_diorite", SpecialBlocks.DIORITE_COBBLESTONE), "cobblestone_diorite");
		GameRegistry.registerBlock(cobblestone_granite = new BlockSpecialBuilder("cobblestone_granite", SpecialBlocks.GRANITE_COBBLESTONE), "cobblestone_granite");
		
		/* other decorative */
		GameRegistry.registerBlock(thatch_block = new BlockThatchBlock("thatch_block"), "thatch_block"); //Misc. Blocks//
		GameRegistry.registerBlock(sod = new BlockSod("sod"), "sod");
		
		/* dynamic */
		GameRegistry.registerBlock(wood_palisade_bottom = new BlockWoodPalisadeBottom("wood_palisade_bottom"), "wood_palisade_bottom"); //Wood Palisade Blocks//	
		GameRegistry.registerBlock(wood_palisade_top = new BlockWoodPalisadeTop("wood_palisade_top"), "wood_palisade_top");
		GameRegistry.registerBlock(wood_palisade = new BlockWoodPalisade("wood_palisade"), "wood_palisade");
		GameRegistry.registerBlock(wall_log_left = new BlockWallLogLeft("wall_log_left"), "wall_log_left"); //Log Wall Blocks//
		GameRegistry.registerBlock(wall_log_middle = new BlockWallLogMiddle("wall_log_middle"), "wall_log_middle");
		GameRegistry.registerBlock(wall_log_right = new BlockWallLogRight("wall_log_right"), "wall_log_right");		
		/* wood */
		GameRegistry.registerBlock(wood_frame = new BlockWoodFrame("wood_frame"), "wood_frame"); //Wood Frame Block//
		/**/
		GameRegistry.registerBlock(wood_slat_bottom = new BlockWoodSlatBottom("wood_slat_bottom"), "wood_slat_bottom"); //Wood Slat Wall Blocks//
		GameRegistry.registerBlock(wood_slat_middle = new BlockWoodSlatMiddle("wood_slat_middle"), "wood_slat_middle");
		GameRegistry.registerBlock(wood_slat_top = new BlockWoodSlatTop("wood_slat_top"), "wood_slat_top");
		/**/
		GameRegistry.registerBlock(wood_slat_birch_bottom = new BlockWoodSlatBirchBottom("wood_slat_birch_bottom"), "wood_slat_birch_bottom"); //Birch Slat Wall Blocks//
		GameRegistry.registerBlock(wood_slat_birch_middle = new BlockWoodSlatBirchMiddle("wood_slat_birch_middle"), "wood_slat_birch_middle");
		GameRegistry.registerBlock(wood_slat_birch_top = new BlockWoodSlatBirchTop("wood_slat_birch_top"), "wood_slat_birch_top");
		/**/
		GameRegistry.registerBlock(wood_slat_acacia_bottom = new BlockWoodSlatAcaciaBottom("wood_slat_acacia_bottom"), "wood_slat_acacia_bottom"); //Acacia Slat Wall Blocks//
		GameRegistry.registerBlock(wood_slat_acacia_middle = new BlockWoodSlatAcaciaMiddle("wood_slat_acacia_middle"), "wood_slat_acacia_middle");
		GameRegistry.registerBlock(wood_slat_acacia_top = new BlockWoodSlatAcaciaTop("wood_slat_acacia_top"), "wood_slat_acacia_top");
		/**/
		GameRegistry.registerBlock(wood_slat_jungle_bottom = new BlockWoodSlatJungleBottom("wood_slat_jungle_bottom"), "wood_slat_jungle_bottom"); //jungle Slat Wall Blocks//
		GameRegistry.registerBlock(wood_slat_jungle_middle = new BlockWoodSlatJungleMiddle("wood_slat_jungle_middle"), "wood_slat_jungle_middle");
		GameRegistry.registerBlock(wood_slat_jungle_top = new BlockWoodSlatJungleTop("wood_slat_jungle_top"), "wood_slat_jungle_top");
		
		/* wood stone */
		GameRegistry.registerBlock(wood_stone = new BlockWoodStone("wood_stone"), "wood_stone"); //Wood Stone Wall Blocks//
		GameRegistry.registerBlock(wood_stone_fancy = new BlockWoodStoneFancy("wood_stone_fancy"), "wood_stone_fancy");
		GameRegistry.registerBlock(wood_stone_r = new BlockWoodStoneR("wood_stone_r"), "wood_stone_r");
		GameRegistry.registerBlock(wood_stone_l = new BlockWoodStoneL("wood_stone_l"), "wood_stone_l");
		GameRegistry.registerBlock(wood_stone_h = new BlockWoodStoneH("wood_stone_h"), "wood_stone_h");
		GameRegistry.registerBlock(wood_stone_v = new BlockWoodStoneV("wood_stone_v"), "wood_stone_v");
		GameRegistry.registerBlock(wood_stone_no_top = new BlockWoodStoneNoTop("wood_stone_no_top"), "wood_stone_no_top");
		GameRegistry.registerBlock(wood_stone_no_bottom = new BlockWoodStoneNoBottom("wood_stone_no_bottom"), "wood_stone_no_bottom");
		GameRegistry.registerBlock(wood_stone_no_left = new BlockWoodStoneNoLeft("wood_stone_no_left"), "wood_stone_no_left");
		GameRegistry.registerBlock(wood_stone_no_right = new BlockWoodStoneNoRight("wood_stone_no_right"), "wood_stone_no_right");
		
		/* tileentity blocks */
		GameRegistry.registerBlock(alloy_furnace = new BlockAlloyFurnace("alloy_furnace"), "alloy_furnace"); //Containers and Crafting Stations//
		GameRegistry.registerBlock(smelting_furnace = new BlockSmeltingFurnace("smelting_furnace"), "smelting_furnace");
		GameRegistry.registerBlock(mixer = new BlockMixer("mixer"), "mixer");
		GameRegistry.registerBlock(brick_oven = new BlockBrickOven("brick_oven"), "brick_oven");
		GameRegistry.registerBlock(wood_stump = new BlockWoodStump("wood_stump"), "wood_stump");
		GameRegistry.registerBlock(butchers_table = new BlockButchersTable("butchers_table"), "butchers_table");
		GameRegistry.registerBlock(artisan_bench = new BlockArtisanBench("artisan_bench"), "artisan_bench");
		GameRegistry.registerBlock(crate = new BlockCrate("crate"), "crate");
		GameRegistry.registerBlock(thatch_basket = new BlockThatchBasket("thatch_basket"), "thatch_basket");
		GameRegistry.registerBlock(camp_fire = new BlockCampFire("camp_fire"), "camp_fire");
		GameRegistry.registerBlock(stonecutters_bench = new BlockStonecuttersBench("stonecutters_bench"), "stonecutters_bench");
		
		/* special purpose blocks */
		GameRegistry.registerBlock(topsoil = new BlockTopsoil("topsoil"), "topsoil");
		
		/* stairs */
		GameRegistry.registerBlock(stairs_thatch = new BlockStairsBuilder(RegisterBlock.thatch_block.getDefaultState(), "thatch_stairs", RegisterItem.thatch, -100, 1, 2), "thatch_stairs");
		GameRegistry.registerBlock(stairs_andesite = new BlockStairsBuilder(Blocks.stone.getStateFromMeta(5), "andesite_stairs", RegisterItem.rock_andesite, -100, 0, 2), "andesite_stairs");
		GameRegistry.registerBlock(stairs_diorite = new BlockStairsBuilder(Blocks.stone.getStateFromMeta(3), "diorite_stairs", RegisterItem.rock_diorite, -100, 0, 2), "diorite_stairs");
		GameRegistry.registerBlock(stairs_granite = new BlockStairsBuilder(Blocks.stone.getStateFromMeta(1), "granite_stairs", RegisterItem.rock_granite, -100, 0, 2), "granite_stairs");
		
		/* plants */
		GameRegistry.registerBlock(jute_plant = new BlockJute("jute_plant"), "jute_plant");
	}
}
