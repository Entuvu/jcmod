package com.entuvu.jcmod.core.proxy;

import com.entuvu.jcmod.JCReferences;
import com.entuvu.jcmod.core.crafting.JCCrafting;
import com.entuvu.jcmod.core.event.JCEventHandler;
import com.entuvu.jcmod.core.network.NetworkController;
import com.entuvu.jcmod.core.register.RegisterBlock;
import com.entuvu.jcmod.core.register.RegisterItem;
import com.entuvu.jcmod.core.register.RegisterTileEntity;
import com.entuvu.jcmod.core.world.MyWorldGenerator;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy{
	public NetworkController networkController;
	
	public void preInit(FMLPreInitializationEvent e) {
		/* Pre-Initialize Network Controller */
		networkController.preInit();
		
		/* Bring in test objects (if TESTMODE is enabled) */
    	if(JCReferences.TESTMODE)
    		TestProxy.CommonPreInit();
    	
		/* Create Items, Blocks, Entities, and TileEntities */
    	RegisterBlock.createBlocks();  /* Blocks need to come first because seeds need to reference blocks */
    	RegisterItem.createItems();
    	RegisterTileEntity.createTileEntities();
    }

    public void init(FMLInitializationEvent e) {
    	/* Initialize Network Controller */
    	networkController.init();
    	
    	if(JCReferences.TESTMODE)
    		TestProxy.CommonInit();
    	
    	JCCrafting.initCrafting();
    	
    	MinecraftForge.EVENT_BUS.register(new JCEventHandler());
        // Register IWorldGenerators
    	GameRegistry.registerWorldGenerator(new MyWorldGenerator(), 0);
    	
    }

    public void postInit(FMLPostInitializationEvent e) {
    	/* POST INIT CODE GOES HERE */
    }
    
    public EntityPlayer getPlayerFromMessageContext(MessageContext ctx)
	{
		return null;
	}
}
