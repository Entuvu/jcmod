package com.entuvu.jcmod.core.proxy;

import com.entuvu.jcmod.JCReferences;
import com.entuvu.jcmod.core.client.render.RenderBlock;
import com.entuvu.jcmod.core.client.render.RenderEntity;
import com.entuvu.jcmod.core.client.render.RenderItem;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ClientProxy extends CommonProxy {

	
    @Override
    public void preInit(FMLPreInitializationEvent e) {
        super.preInit(e);  /* Call Common Proxy Register (Create) */
        RenderEntity.preInit();
    }

    @Override
    public void init(FMLInitializationEvent e) {
        super.init(e);
        /* For test objects.  We can disable test mode in references, and therefore remove all test objects */
        if(JCReferences.TESTMODE)
        	TestProxy.ClientInit();
        
        RenderItem.registerItemRenderer();
        RenderBlock.renderBlocks();
        
        
    }

    @Override
    public void postInit(FMLPostInitializationEvent e) {
        super.postInit(e);
    }
    
    @Override
    public EntityPlayer getPlayerFromMessageContext(MessageContext ctx)
	{
		switch (ctx.side)
		{
			case CLIENT:
			{
				return Minecraft.getMinecraft().thePlayer;
			}
			case SERVER:
			{
				return ctx.getServerHandler().playerEntity;
			}
			default:
				System.out.println("Invalid side in TestMsgHandler: " + ctx.side);
		}
		return null;
	}
}
